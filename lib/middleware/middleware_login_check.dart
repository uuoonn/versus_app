import 'package:flutter/material.dart';
import 'package:versus_app/pages/bottom_bar_menu/page_index.dart';
import 'package:versus_app/pages/bottom_bar_menu/page_search.dart';
import '../functions/token_lib.dart';
import '../pages/start/page_login.dart';


class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    String? memberToken = await TokenLib.getMemberToken();
    if (memberToken == null) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageLogin()),
              (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => PageIndex()),
              (route) => false);
    }
  }
}