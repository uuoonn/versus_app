import 'login_response.dart';

class LoginResult {
  String msg;
  int code;
  LoginResponse data;

  LoginResult(
      this.msg,
      this.code,
      this.data,);

  factory LoginResult.fromJson(Map<String, dynamic> json) {
    return LoginResult(
      json['msg'],
      json['code'],
      LoginResponse.fromJson(json['data']),
    );
  }
}