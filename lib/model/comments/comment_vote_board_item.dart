class CommentVoteBoardItem {
  num id;
  String memberNickname;
  bool voteBoardIsA;
  num recommendNum;
  String dateCommentRegister;

  CommentVoteBoardItem(
      this.id,
      this.memberNickname,
      this.voteBoardIsA,
      this.recommendNum,
      this.dateCommentRegister,
      );

  factory CommentVoteBoardItem.fromJson(Map<String, dynamic> json) {
    return CommentVoteBoardItem (
      json['id'],
      json['memberNickname'],
      json['voteBoardIsA'],
      json['recommendNum'],
      json['dateCommentRegister'],
    );
  }


}