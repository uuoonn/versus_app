import 'package:versus_app/model/comments/comment_vote_board_item.dart';

class CommentListResult {
  String msg;
  num code;
  List<CommentVoteBoardItem> list;
  num totalCount;
  num totalPage;
  num currentPage;

  CommentListResult(this.msg, this.code, this.list, this.totalCount, this.totalPage, this.currentPage);

  factory CommentListResult.fromJson(Map<String, dynamic> json) {
    return CommentListResult(
      json['msg'],
      json['code'],
      json['list'] != null ?
      (json['list'] as List).map((e) => CommentVoteBoardItem.fromJson(e)).toList()
          : [],
      json['totalCount'],
      json['totalPage'],
      json['currentPage'],
    );
  }
}