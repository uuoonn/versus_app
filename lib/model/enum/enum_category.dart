enum EnumCategory {
  DAILY('DAILY','데일리'),
  HUMOR('HUMOR','유머'),
  INFO('INFO','정보'),
  ENTERTAINMENT('ENTERTAINMENT','연예'),
  GAME('GAME','게임'),
  ANIMATION('ANIMATION','만화'),
  SPORTS('SPORTS','스포츠');

  const EnumCategory(this.code, this.name);
  final String code;
  final String name;

  factory EnumCategory.getByCode(String code) {
    return EnumCategory.values.firstWhere((value) => value.code == code);
  }

}