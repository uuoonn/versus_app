import 'board_item.dart';

class BoardTopListResult {
  String msg;
  num code;
  List<BoardItem> list;
  num totalCount;

  BoardTopListResult(this.msg, this.code, this.list, this.totalCount,);

  factory BoardTopListResult.fromJson(Map<String, dynamic> json) {
    return BoardTopListResult(
      json['msg'],
      json['code'],
      json['list'] != null ?
      (json['list'] as List).map((e) => BoardItem.fromJson(e)).toList()
          : [],
      json['totalCount'],
    );
  }
}