import 'board_item.dart';

class BoardListResult {
  String msg;
  num code;
  List<BoardItem> list;
  num totalCount;
  num totalPage;
  num currentPage;

  BoardListResult(this.msg, this.code, this.list, this.totalCount, this.totalPage, this.currentPage);

  factory BoardListResult.fromJson(Map<String, dynamic> json) {
    return BoardListResult(
      json['msg'],
      json['code'],
      json['list'] != null ?
      (json['list'] as List).map((e) => BoardItem.fromJson(e)).toList()
          : [],
      json['totalCount'],
      json['totalPage'],
      json['currentPage'],
    );
  }
}