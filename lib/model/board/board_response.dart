//보드 상세보기 단수 R
class BoardResponse {
  num? id;
  String memberNickName;
  String categoryName;
  String postTypeName;
  String topicA;
  String contentA;
  num? contentsANum;
  String topicB;
  String contentB;
  num? contentsBNum;
  String dateBoard;
  num viewCount;
  num likeCount;
  String isOverTimeName;

  BoardResponse(
      this.id,
      this.memberNickName,
      this.categoryName,
      this.postTypeName,
      this.topicA,
      this.contentA,
      this.contentsANum,
      this.topicB,
      this.contentB,
      this.contentsBNum,
      this.dateBoard,
      this.viewCount,
      this.likeCount,
      this.isOverTimeName,
      );
  factory BoardResponse.fromJson(Map<String, dynamic> json) {
    return BoardResponse(
      json['id'],
      json['memberNickName'],
      json['categoryName'],
      json['postTypeName'],
      json['topicA'],
      json['contentA'],
      json['contentsANum'],
      json['topicB'],
      json['contentB'],
      json['contentsBNum'],
      json['dateBoard'],
      json['viewCount'],
      json['likeCount'],
      json['isOverTimeName'],
    );
  }
}