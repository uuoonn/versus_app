import 'package:versus_app/model/board/board_response.dart';

class BoardDetailResult {
  String msg;
  num code;
  BoardResponse data;

  BoardDetailResult(this.msg, this.code, this.data);

  factory BoardDetailResult.fromJson(Map<String, dynamic> json) {
    return BoardDetailResult(
      json['msg'],
      json['code'],
      BoardResponse.fromJson(json['data'])
    );
  }


}