import 'package:versus_app/model/vote/vote_board_item.dart';

class VoteListResult {
  String msg;
  num code;
  List<VoteBoardItem> list;
  int totalCount;


  VoteListResult(
      this.msg, this.code, this.list, this.totalCount,);

  factory VoteListResult.fromJson(Map<String, dynamic> json) {
    return VoteListResult(
      json['msg'],
      json['code'],
      json['list'] != null ?
      (json['list'] as List).map((e) => VoteBoardItem.fromJson(e)).toList()
          : [],
      json['totalCount'],

    );
  }
}