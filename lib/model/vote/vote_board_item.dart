class VoteBoardItem {
  num id;
  num memberId;
  String memberName;
  num boardId;
  String boardTopicA;
  String boardTopicB;
  String isAName;
  String dateVote;

  VoteBoardItem(
      this.id,
      this.memberId,
      this.memberName,
      this.boardId,
      this.boardTopicA,
      this.boardTopicB,
      this.isAName,
      this.dateVote,
      );

  factory VoteBoardItem.fromJson(Map<String, dynamic> json) {
    return VoteBoardItem(
        json['id'],
        json['memberId'],
        json['memberName'],
        json['boardId'],
        json['boardTopicA'],
        json['boardTopicB'],
        json['isAName'],
        json['dateVote'],
    );
  }

}