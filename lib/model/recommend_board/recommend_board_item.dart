class RecommendBoardItem {
  num id;
  String memberNickName;
  num boardId;
  String boardTopicA;
  String boardTopicB;
  String boardDateBoard;

  RecommendBoardItem(
      this.id,
      this.memberNickName,
      this.boardId,
      this.boardTopicA,
      this.boardTopicB,
      this.boardDateBoard,
      );

  factory RecommendBoardItem.fromJson(Map<String, dynamic> json) {
    return RecommendBoardItem(
      json['id'],
      json['memberNickName'],
      json['boardId'],
      json['boardTopicA'],
      json['boardTopicB'],
      json['boardDateBoard'],
    );
  }


}