import 'package:versus_app/model/recommend_board/recommend_board_item.dart';

class RecommendBoardListResult {
  String msg;
  num code;
  List<RecommendBoardItem> list;
  int totalCount;


  RecommendBoardListResult(
      this.msg, this.code, this.list, this.totalCount,);

  factory RecommendBoardListResult.fromJson(Map<String, dynamic> json) {
    return RecommendBoardListResult(
      json['msg'],
      json['code'],
      json['list'] != null ?
      (json['list'] as List).map((e) => RecommendBoardItem.fromJson(e)).toList()
          : [],
      json['totalCount'],

    );
  }
}