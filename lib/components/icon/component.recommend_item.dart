import 'package:flutter/material.dart';

class ComponentRecommendItem extends StatefulWidget {
  const ComponentRecommendItem({super.key});

  @override
  State<ComponentRecommendItem> createState() => _ComponentRecommendItem();
}

class _ComponentRecommendItem extends State<ComponentRecommendItem> {
  bool state = false;

  Icon get icon {
    final IconData iconData = state ? Icons.star : Icons.star_outline;

    return Icon(
      iconData,
      color: Colors.amber,
      size: 25,
    );
  }

  void _toggle() {
    setState(() {
      state = !state;
    });
  }

  double get turns => state ? 1 : 0;


  @override
  Widget build(BuildContext context) {
    return AnimatedRotation(
      turns: turns,
      curve: Curves.decelerate,
      duration: const Duration(milliseconds: 300),
      child: FloatingActionButton(
        elevation: 0,
        shape: const CircleBorder(),
        backgroundColor: Colors.white,
        onPressed: () => _toggle(),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: icon,
        ),
      ),
    );
  }
}