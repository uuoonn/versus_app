import 'package:flutter/material.dart';

class ComponentCommentItem extends StatefulWidget {
  const ComponentCommentItem({
    super.key,
    required this.callback,
  });

  final VoidCallback callback;

  @override
  State<ComponentCommentItem> createState() => _ComponentCommentItem();
}

class _ComponentCommentItem extends State<ComponentCommentItem> {
  bool state = false;

  Icon get icon {
    final IconData iconData = state ? Icons.mode_comment : Icons.mode_comment_outlined;

    return Icon(
      iconData,
      color: Colors.grey,
      size: 25,
    );
  }

  void _toggle() {
    setState(() {
      state = !state;
    });
  }

  double get turns => state ? 1 : 0;


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.callback,
      child: AnimatedRotation(
        turns: turns,
        curve: Curves.decelerate,
        duration: const Duration(milliseconds: 300),
        child: FloatingActionButton(
          elevation: 0,
          shape: const CircleBorder(),
          backgroundColor: Colors.white,
          onPressed: () => _toggle(),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: icon,
          ),
        ),
      ),
    );
  }
}