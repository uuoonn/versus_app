import 'package:flutter/material.dart';

class ComponentCountTitle extends StatelessWidget {
  const ComponentCountTitle({
    super.key,
    required this.count,
    required this.unitName,
    required this.itemName,
  });

  final int count;
  final String unitName;
  final String itemName;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Text('총 ${count.toString()} $unitName의 $itemName이(가) 있습니다.')
    );
  }
}
