import 'package:flutter/material.dart';

class ComponentListItem extends StatelessWidget {
  const ComponentListItem({
    super.key,
    required this.color,
    required this.title,
    required this.fontSize,
    required this.callback,
    required this.fontWeight,
  });

  final Color color;
  final String title;
  final VoidCallback callback;
  final double fontSize;
  final FontWeight fontWeight;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
          title,
        style: TextStyle(
          fontSize: fontSize,
          fontWeight: fontWeight,
          color: color,
        ),
      ),
      onTap: callback,
    );
  }
}
