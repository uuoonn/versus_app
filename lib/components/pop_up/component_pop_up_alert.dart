import 'package:flutter/material.dart';
import 'package:versus_app/config/size.dart';

class ComponentPopUpAlert extends StatelessWidget {
  const ComponentPopUpAlert({
    super.key,
    required this.title,
    required this.contents,
    required this.text,
    required this.callback,
  });

  final String title;
  final String contents;
  final String text;
  final VoidCallback callback;



  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Center(
        child: Text(
          title,
          style: const TextStyle(
            fontSize: fontSizeLarge,
          ),
        ),
      ),
      content: Text(
        contents,
        style: const TextStyle(
            fontSize: fontSizeSmall
        ),
      ),

      actions: <Widget>[
        SizedBox(
          width: 80,
          height: 46,
          child: TextButton(
            style: TextButton.styleFrom(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(circularRadiusBase)
                )
            ),
            onPressed: callback,
            child: Text(text),),
        ),
      ],
    );
  }
}
