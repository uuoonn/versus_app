import 'package:flutter/material.dart';
import 'package:versus_app/config/size.dart';

class ComponentPopUpChoice extends StatelessWidget {
  const ComponentPopUpChoice({
    super.key,
    required this.title,
    required this.contents,
    required this.text1,
    required this.callback1,
    required this.text2,
    required this.callback2,
  });

  final String title;
  final String contents;
  final String text1;
  final VoidCallback callback1;
  final String text2;
  final VoidCallback callback2;


  @override
  Widget build(BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Text(
                    title,
                  style: const TextStyle(
                    fontSize: fontSizeLarge,
                  ),
                ),
              ),
              content: Text(
                  contents,
                style: const TextStyle(
                    fontSize: fontSizeSmall
                ),
              ),

              actions: <Widget>[
                SizedBox(
                  width: 80,
                  height: 46,
                  child: TextButton(
                    style: TextButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(circularRadiusBase)
                        )
                    ),
                    onPressed: callback1,
                    child: Text(text1),),
                ),
                SizedBox(
                  width: 80,
                  height: 46,
                  child: TextButton(
                    style: TextButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(circularRadiusBase)
                        )
                    ),
                    onPressed: callback2,
                    child: Text(text2),),
                ),
              ],
            );
          }
    }
