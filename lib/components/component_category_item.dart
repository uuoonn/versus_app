import 'package:flutter/material.dart';
import 'package:versus_app/config/size.dart';
import 'package:versus_app/model/enum/enum_category.dart';

class ComponentCategoryItem extends StatefulWidget {
  const ComponentCategoryItem({
    super.key,
    required this.categoryItem,
    required this.callback,
  });

  final EnumCategory categoryItem;
  final VoidCallback callback;

  @override
  State<ComponentCategoryItem> createState() => _ComponentCategoryItemState();
}

class _ComponentCategoryItemState extends State<ComponentCategoryItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.callback,
      child: Container(
        decoration: BoxDecoration(
          //그라데이션 효과
          gradient: const LinearGradient(
            begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [Colors.redAccent,Colors.blueAccent,],
          ),

          //보더 라운드 20
          borderRadius: BorderRadius.circular(circularRadiusBase),
        ),


        child: Container(
          padding: EdgeInsets.fromLTRB(0, 80, 20, 0),
          child: Text(
            widget.categoryItem.name,
            textAlign: TextAlign.end,
            style: const TextStyle(
              fontSize: fontSizeXlarge,
              fontWeight: FontWeight.w500,
              color: Colors.white
            ),
          ),
        ),
      ),
    );
  }
}
