import 'package:chat_bubbles/bubbles/bubble_special_one.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:versus_app/model/comments/comment_vote_board_item.dart';

class ComponentChatBubble extends StatelessWidget {
  const ComponentChatBubble({Key? key, required this.item})
      : super(key: key);

  final CommentVoteBoardItem item;


  @override
  Widget build(BuildContext context) {
    DateTime dateTime = DateTime.parse(item.dateCommentRegister);
    return Row(
        mainAxisAlignment:
        item.voteBoardIsA ? MainAxisAlignment.start : MainAxisAlignment.end,
        children: [
          if (item.voteBoardIsA) ...{

            Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      BubbleSpecialOne(
                        text: 'ssaaassdsaa',
                        isSender: false,
                        color: Colors.yellow,
                      ),
                      Text('${DateFormat('yyyy년mm월dd일 hh시mm분').format(dateTime)}'),
                    ]))

          },
          if (!item.voteBoardIsA)
            Padding(
                padding: const EdgeInsets.only(bottom: 15),
                child: Padding(
                          padding: const EdgeInsets.only(left: 40),
                          child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                BubbleSpecialOne(
                                  text: 'ddddddd',
                                  isSender: false,
                                  color: Colors.yellow,
                                ),
                                Text('${DateFormat('yyyy년mm월dd일 hh시mm분').format(dateTime)}'),
                              ]))
            )]
    );
  }}