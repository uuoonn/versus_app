import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:versus_app/model/recommend_board/recommend_board_item.dart';

import '../../config/path.dart';
import '../../config/size.dart';


class ComponentPreviewRecommendItem extends StatefulWidget {
  const ComponentPreviewRecommendItem({
    super.key,
    required this.recommendBoardItem,
    required this.callback,

  });
  final RecommendBoardItem recommendBoardItem;
  final VoidCallback callback;

  @override
  State<ComponentPreviewRecommendItem> createState() => _ComponentPreviewRecommendItemState();
}

class _ComponentPreviewRecommendItemState extends State<ComponentPreviewRecommendItem> {
  @override
  Widget build(BuildContext context) {
    //시간 재가공
    DateTime dateTime = DateTime.parse(widget.recommendBoardItem.boardDateBoard);
    return GestureDetector(
      onTap: widget.callback,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(circularRadiusSmall),
            border: Border.all(
              color: const Color.fromRGBO(241, 234, 245, 1),
              width: 2.0,
            )
        ),
        padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: const Color.fromRGBO(255, 255, 30, 0.1),
              child: Column(
                children: [

                  // 토픽A
                  Container(
                    alignment: Alignment.center,
                    height: 50,
                    child: Text(
                      widget.recommendBoardItem.boardTopicA,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                          fontWeight: FontWeight.w500
                      ),
                    ),
                  ),

                  // VS 로고
                  SizedBox(
                    child: Center(
                      child: Image.asset(
                        '${pathLogo}logo_black.png',
                        width: 20,
                        height: 20,
                      ),
                    ),
                  ),

                  // 토픽 B
                  Container(
                    height: 50,
                    alignment: Alignment.center,
                    child: Text(
                      widget.recommendBoardItem.boardTopicB,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: const TextStyle(
                          fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: mediumGap,),
            LayoutBuilder(builder: (context, constraints) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(width: smallGap,),

                        // 투표 시간
                        Text(
                          '${widget.recommendBoardItem.memberNickName}님이 좋아요 한 날 : ${DateFormat('y년M월d일 hh시mm분').format(dateTime)}',
                          overflow: TextOverflow.fade,
                          maxLines: 1,
                          style: const TextStyle(
                            fontSize: fontSizeMicro,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            }),
          ],
        ),
      ),
    );
  }
}
