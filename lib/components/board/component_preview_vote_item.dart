import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:versus_app/config/path.dart';
import 'package:versus_app/model/vote/vote_board_item.dart';

import '../../config/size.dart';

class ComponentPreviewVoteItem extends StatefulWidget {
  const ComponentPreviewVoteItem({
    super.key, required this.voteBoardItem, required this.callback,
  });

  final VoteBoardItem voteBoardItem;
  final VoidCallback callback;

  @override
  State<ComponentPreviewVoteItem> createState() => _ComponentPreviewVoteItemState();
}

class _ComponentPreviewVoteItemState extends State<ComponentPreviewVoteItem> {
  @override
  Widget build(BuildContext context) {
    //시간 재가공
    DateTime dateTime = DateTime.parse(widget.voteBoardItem.dateVote);
    return GestureDetector(
      onTap: widget.callback,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(circularRadiusSmall),
            border: Border.all(
              color: const Color.fromRGBO(241, 234, 245, 1),
              width: 2.0,
            )
        ),
        padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
        child: Column(
          children: [
            Container(
              color: const Color.fromRGBO(255, 255, 30, 0.1),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                  // 토픽A
                  Container(
                    alignment: Alignment.center,
                    height: 50,
                    child: Text(
                      widget.voteBoardItem.boardTopicA,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                          fontWeight: FontWeight.w500
                      ),
                    ),
                  ),

                  // VS 로고
                  SizedBox(
                    child: Center(
                      child: Image.asset(
                        '${pathLogo}logo_black.png',
                        width: 20,
                        height: 20,
                      ),
                    ),
                  ),

                  // 토픽 B
                  Container(
                    height: 50,
                    alignment: Alignment.center,
                    child: Text(
                      widget.voteBoardItem.boardTopicB,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: const TextStyle(
                          fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                ],
              ),
            ),
            LayoutBuilder(builder: (context, constraints) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(width: smallGap,),

                        // 투표 시간
                        Center(
                          child: Text(
                            '${widget.voteBoardItem.memberName}님의 선택',
                            overflow: TextOverflow.fade,
                            maxLines: 1,
                            style: const TextStyle(
                              fontSize: fontSizeMedium,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Center(
                          //유저가 선택한 투표 결과 내용
                          child: Text(
                            '<${widget.voteBoardItem.isAName == 'A투표' ? widget.voteBoardItem.boardTopicA : widget.voteBoardItem.boardTopicB}>',
                            overflow: TextOverflow.fade,
                            maxLines: 1,
                            style: const TextStyle(
                              fontSize: fontSizeMedium,
                              fontWeight: FontWeight.bold,
                                backgroundColor: Color.fromRGBO(
                                    255, 255, 30, 0.4)
                            ),
                          ),
                        ),
                        const SizedBox(height: mediumGap,),

                        // 투표 시간
                        Center(
                          child: Text(
                            '${widget.voteBoardItem.memberName}님이 투표한 날 : ${DateFormat('y년M월d일 hh시mm분').format(dateTime)}',
                            overflow: TextOverflow.fade,
                            maxLines: 1,
                            style: const TextStyle(
                              fontSize: fontSizeMicro,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            }),
            const SizedBox(height: mediumGap,)
          ],
        ),
      ),
    );
  }
}
