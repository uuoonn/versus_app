import 'package:flutter/material.dart';
import 'package:versus_app/config/size.dart';
import 'package:versus_app/model/board/board_response.dart';

class ComponentChoiceBBox extends StatefulWidget {
  const ComponentChoiceBBox({
    Key? key,
    required this.boardResponse,
    required this.callback,
  }) : super(key: key);

  final BoardResponse boardResponse;
  final VoidCallback callback;

  @override
  State<ComponentChoiceBBox> createState() => _ComponentChoiceBBoxState();
}

class _ComponentChoiceBBoxState extends State<ComponentChoiceBBox> {
  bool _isClicked = false; // 클릭 여부를 저장하는 변수 추가

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 2,
      alignment: Alignment.bottomCenter,
      child: Stack(
        alignment: Alignment.center,
        children: [
          GestureDetector(
            onTap: () {
              setState(() {
                _isClicked = true; // 클릭될 때 _isClicked 값을 true로 변경
              });
              widget.callback(); // 콜백 호출
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text('회원님의 선택! \"${widget.boardResponse.topicB}\"'),
                backgroundColor: Colors.blueAccent,
                duration: Duration(milliseconds: 1000),
              ));
            },
            child: Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width - 50,
              height: MediaQuery.of(context).size.height / 2 - 180,
              decoration: BoxDecoration(
                border: Border.all(
                    width:  0,
                    color: _isClicked ? Color.fromRGBO(0, 0, 0, 0) : Color.fromRGBO(255, 255, 255, 0) ),
                borderRadius: BorderRadius.circular(10),
                color: _isClicked ? Colors.blue[200] : Colors.transparent,
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.22,
            padding: EdgeInsets.all(10),
            alignment: Alignment.center,
            child: Text(
              widget.boardResponse.topicB,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: fontSizeXlarge,
                color: Color.fromRGBO(0, 0, 0, 1.0),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
