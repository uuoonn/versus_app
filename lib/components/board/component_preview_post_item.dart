import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:versus_app/config/path.dart';
import 'package:versus_app/model/board/board_item.dart';

import '../../config/size.dart';

class ComponentPreviewPostItem extends StatefulWidget {
  const ComponentPreviewPostItem({
    super.key,
    required this.boardItem,
    required this.callback});

  final BoardItem boardItem;
  final VoidCallback callback;

  @override
  State<ComponentPreviewPostItem> createState() => _ComponentPreviewPostItemState();
}

class _ComponentPreviewPostItemState extends State<ComponentPreviewPostItem> {
  @override
  Widget build(BuildContext context) {
    //백엔드에서 받은 시간 재가공
    DateTime dateTime = DateTime.parse(widget.boardItem.dateBoard);
    return GestureDetector(
      onTap: widget.callback,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(circularRadiusSmall),
            border: Border.all(
              color: const Color.fromRGBO(241, 234, 245, 1),
              width: 2.0,
            )
        ),
        padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: const Color.fromRGBO(255, 255, 30, 0.1),
              child: Column(
                children: [

                  // 토픽A
                  Container(
                    alignment: Alignment.center,
                    height: 50,
                    child: Text(
                      widget.boardItem.topicA,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                          fontWeight: FontWeight.w500
                      ),
                    ),
                  ),

                  // VS 로고
                  SizedBox(
                    child: Center(
                      child: Image.asset(
                        '${pathLogo}logo_black.png',
                        width: 20,
                        height: 20,
                      ),
                    ),
                  ),

                  // 토픽 B
                  Container(
                    height: 50,
                    alignment: Alignment.center,
                    child: Text(
                      widget.boardItem.topicB,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: const TextStyle(
                          fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: mediumGap,),
            LayoutBuilder(builder: (context, constraints) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                  // 유저 프로필 이미지
                  CircleAvatar(
                    backgroundColor: Colors.white,
                    child : Image.network(widget.boardItem.memberImgUrl),
                  ),
                  // const Padding(padding: EdgeInsets.symmetric(horizontal: 6.0)),

                  const SizedBox(width: smallGap,),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(width: smallGap,),

                        //닉네임
                        Text(
                          widget.boardItem.memberNickName,
                          overflow: TextOverflow.fade,
                          maxLines: 1,
                        ),
                        const SizedBox(width: smallGap,),
                        const SizedBox(width: smallGap,),

                        // 투표 시간
                        Text(
                          '작성 일시 : ${DateFormat('y년M월d일 hh시mm분').format(dateTime)}',
                          overflow: TextOverflow.fade,
                          maxLines: 1,
                          style: const TextStyle(
                            fontSize: fontSizeMicro,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            }),
            const SizedBox(height: mediumGap,)
          ],
        ),
      ),
    );
  }
}
