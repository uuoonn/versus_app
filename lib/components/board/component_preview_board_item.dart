//게시글 복수R 리스트 컴포넌트
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:versus_app/config/path.dart';
import 'package:versus_app/config/size.dart';
import 'package:versus_app/model/board/board_item.dart';


class ComponentPreviewBoardItem extends StatefulWidget {
  const ComponentPreviewBoardItem({
    super.key,
    required this.boardItem,
    required this.callback,

  });

  final BoardItem boardItem;
  final VoidCallback callback;

  @override
  State<ComponentPreviewBoardItem> createState() => _ComponentPreviewBoardItemState();
}

class _ComponentPreviewBoardItemState extends State<ComponentPreviewBoardItem> {
  // 추천수, 댓글수 받아와야 함 - 백엔드 수정 후 받아오기
  @override
  Widget build(BuildContext context) {
    //백엔드에서 받은 시간 재가공
    DateTime dateTime = DateTime.parse(widget.boardItem.dateBoard);

    return GestureDetector(
      onTap: widget.callback,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(circularRadiusSmall),
          border: Border.all(
            color: const Color.fromRGBO(241, 234, 245, 1),
            width: 2.0,
          )
        ),
        padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              children: [
                // 토픽A
                Container(
                  alignment: Alignment.center,
                  height: 50,
                  child: Text(
                    widget.boardItem.topicA,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        fontWeight: FontWeight.w500
                    ),
                  ),
                ),
                // VS 로고
                SizedBox(
                  child: Center(
                    child: Image.asset(
                      '${pathLogo}logo_black.png',
                      width: 20,
                      height: 20,
                    ),
                  ),
                ),
                // 토픽 B
                Container(
                  height: 50,
                  alignment: Alignment.center,
                  child: Text(
                    widget.boardItem.topicB,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: const TextStyle(
                        fontWeight: FontWeight.w500
                    ),
                  ),
                ),
              ],
            ),
            LayoutBuilder(builder: (context, constraints) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                    // 유저 프로필 이미지
                    CircleAvatar(
                      backgroundColor: Colors.white,
                      child : Image.network(widget.boardItem.memberImgUrl),
                    ),

                  const SizedBox(width: smallGap,),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(width: smallGap,),

                        //닉네임
                        Text(
                          widget.boardItem.memberNickName,
                          overflow: TextOverflow.fade,
                          maxLines: 1,
                        ),
                        const SizedBox(width: smallGap,),

                        // 작성 시간
                        Text(DateFormat('y년M월d일 hh시mm분').format(dateTime),
                          overflow: TextOverflow.fade,
                          maxLines: 1,
                          style: const TextStyle(
                            fontSize: fontSizeMicro,
                          ),
                        )

                      ],
                    ),
                  ),
                  Column(

                    children: [

                      //조회 아이콘, 조회 수
                      const Icon(
                        Icons.remove_red_eye_outlined,
                        size: 20.0,
                        color: Color.fromRGBO(0, 0, 0, 0.4),
                      ),
                      Text(
                        '${widget.boardItem.viewCount}',
                        style: const TextStyle(
                          fontWeight: FontWeight.w100,
                          fontSize: fontSizeMicro,
                          color: Color.fromRGBO(111, 111, 111, 1.0),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(width: largeGap,),
                  Column(
                    children: [
                      //추천 아이콘, 추천 수
                      const Icon(
                        Icons.thumb_up_outlined,
                        size: 20.0,
                        color: Color.fromRGBO(0, 0, 0, 0.4),
                      ),
                      Text(
                        '${widget.boardItem.likeCount}', // 받아와야 함
                        style: const TextStyle(
                          fontWeight: FontWeight.w100,
                          fontSize: fontSizeMicro,
                          color: Color.fromRGBO(111, 111, 111, 1.0),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(width: smallGap,),

                ],
              );
            }),
            const SizedBox(height: mediumGap,)
          ],
        ),
      ),
    );
  }
}
