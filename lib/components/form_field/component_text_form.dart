import 'package:flutter/material.dart';
import 'package:versus_app/config/size.dart';

class ComponentTextForm extends StatefulWidget {
  const ComponentTextForm({
    super.key,
    required this.title,
    required this.width,
    required this.height,
    required this.obscureText,
    required this.hintText,
    required this.lines,
  });

  final String title;
  final String hintText;
  final double width;
  final double height;
  final bool obscureText;
  final int lines;

  @override
  State<ComponentTextForm> createState() => _ComponentTextFormState();
}

class _ComponentTextFormState extends State<ComponentTextForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
            widget.title,
          style: const TextStyle(
            fontSize: fontSizeMedium,
          ),
        ),
        const SizedBox(height: smallGap,),
        Container(
          alignment: Alignment.centerLeft,
          width: widget.width,
          height: widget.height,
          child: TextFormField(
            maxLines: widget.lines,
            obscureText: widget.obscureText, // 입력시 ** 처리
            decoration: InputDecoration(
              hintStyle: const TextStyle(
                fontSize: fontSizeMedium,
              ),
              hintText: widget.hintText,
              enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(circularRadiusBase),
            ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(circularRadiusBase),
              ),
              errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(circularRadiusBase),
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(circularRadiusBase),
              ),
          ),
          ),
        ),
      ],
    );
  }
}
