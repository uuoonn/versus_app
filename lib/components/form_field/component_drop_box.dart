import 'package:flutter/material.dart';
import 'package:versus_app/config/size.dart';

class ComponentDropBox extends StatefulWidget {
  const ComponentDropBox({
    super.key,
    required this.hintTitle,
    required this.width,
    required this.height,
  });

  final String hintTitle;
  final double width;
  final double height;

  @override
  State<ComponentDropBox> createState() => _ComponentDropBoxState();
}

class _ComponentDropBoxState extends State<ComponentDropBox> {

  List<String> list = []; // 가져오는 값 어떻게?
  String? selectedValue;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: widget.height,
      width: widget.width,
      child: DropdownButtonFormField(
        isExpanded: true,
        borderRadius: BorderRadius.circular(20),
        decoration: InputDecoration(
            contentPadding: const EdgeInsets.symmetric(vertical: 20),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            )
        ),
        hint: Container(
          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Text (
            widget.hintTitle,
            style: const TextStyle(fontSize: fontSizeSmall),
          ),
        ),
        items: list.map((String item) {
          return DropdownMenuItem<String>(
            value: item,
            child: Text(
              item,
              style: const TextStyle(
                fontSize: fontSizeSmall,
              ),
            ),
          );
        }).toList(),
        validator: (value) {
          if (value == null) {
            return widget.hintTitle;
          }
          return null;
        },
        onChanged: (dynamic value) {},
        onSaved: (value){
          selectedValue = value.toString();
        },

      ),
    );
  }
}
