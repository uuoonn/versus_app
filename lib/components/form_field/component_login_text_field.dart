import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

import '../../config/size.dart';

class ComponentLoginTextField extends StatefulWidget {
  const ComponentLoginTextField({super.key, required this.title, required this.hintText, required this.width, required this.obscureText, required this.name});

  final String title;
  final String hintText;
  final double width;
  //final double height;
  final bool obscureText;
  final String name;

  @override
  State<ComponentLoginTextField> createState() => _ComponentLoginTextFieldState();
}

class _ComponentLoginTextFieldState extends State<ComponentLoginTextField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.title,
          style: const TextStyle(
            fontSize: fontSizeMedium,
          ),
        ),
        const SizedBox(height: smallGap,),
        Container(
          alignment: Alignment.centerLeft,
          width: widget.width,

          child: FormBuilderTextField(
            keyboardType: TextInputType.text,
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(errorText: '다시 입력해주세요.'),
              FormBuilderValidators.minLength(6, errorText: '6자 이상 입력해주세요.'),
              FormBuilderValidators.maxLength(12, errorText: '12자 이하로 입력해주세요.'),
            ]),
            obscureText: widget.obscureText, // 입력시 ** 처리
            decoration: InputDecoration(
              hintStyle: const TextStyle(
                fontSize: fontSizeMedium,
              ),
              hintText: widget.hintText,
              //포커스 되지 않은 상태 (기본 상태)
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(circularRadiusBase),
              ),
              //포커스 된 상태 (필드 활성화)
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(circularRadiusBase),
                borderSide: BorderSide(width: 2.0)
              ),
              //오류가 난 상태
              errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(circularRadiusBase),
                borderSide: BorderSide(color: Colors.red,width: 2.0)
              ),
              //포커스 된 상태에서 오류가 난 상태(포커스 된 상태에서 입력 값이 유효하지 않은 경우)
              focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(circularRadiusBase),
                  borderSide: BorderSide(color: Colors.red,width: 2.0)
              ),
            ), name: widget.name,
          ),
        ),
      ],
    );
  }
}
