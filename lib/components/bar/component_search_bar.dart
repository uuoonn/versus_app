import 'package:flutter/material.dart';



class ComponentSearchBar extends StatelessWidget {
  const ComponentSearchBar({
    super.key,
  });

  // 검색 영역 받아올 값 모델 생성하고 넣어주기
  //미사용 중임



  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 56,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          color: Colors.white,
        ),
        padding: const EdgeInsets.fromLTRB(31, 12, 12, 12),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(width: 23.5),
            Expanded(
              child: TextField(
                maxLines: 1,
                decoration: InputDecoration(
                  isDense: true,
                  border: InputBorder.none,
                  hintText: '댓글을 입력하세요.',
                  hintStyle: Theme.of(context).textTheme.bodyMedium,
                ),
              ),
            ),
            const Icon(Icons.send)
          ],
        ),
      ),
    );
  }
}