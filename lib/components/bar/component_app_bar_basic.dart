import 'package:flutter/material.dart';
import 'package:versus_app/config/path.dart';
import 'package:versus_app/config/size.dart';

class ComponentAppBarBasic extends StatelessWidget implements PreferredSizeWidget {
  const ComponentAppBarBasic({
    super.key,
    required this.text,
    required this.onLeadingPressed,

  });

  final String text;
  final VoidCallback? onLeadingPressed;

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);


  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: Container(
        margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
        child: IconButton(
            onPressed: onLeadingPressed,
            icon: Image.asset(
                '${pathLogo}logo_black.png',

            )),
      ),
      title:
      Text(
        text,
        style: const TextStyle(
          fontSize: fontSizeXlarge,
          fontWeight: FontWeight.bold,
        ),
      ),
      backgroundColor: const Color.fromRGBO(0, 0, 0, 0),
    );
  }
}
