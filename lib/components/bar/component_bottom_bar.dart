import 'package:flutter/material.dart';
import 'package:versus_app/pages/bottom_bar_menu/page_category.dart';
import 'package:versus_app/pages/bottom_bar_menu/page_create_post.dart';
import 'package:versus_app/pages/bottom_bar_menu/page_feed.dart';
import 'package:versus_app/pages/bottom_bar_menu/page_my_board.dart';
import 'package:versus_app/pages/bottom_bar_menu/page_search.dart';

import '../../model/member/member_response.dart';
import '../../repository/repo_member.dart';


class ComponentBottomBar extends StatefulWidget {
  const ComponentBottomBar({
    super.key,
    required this.id,
  });
  final num id;

  @override
  State<ComponentBottomBar> createState() => _ComponentBottomBarState();
}

class _ComponentBottomBarState extends State<ComponentBottomBar> {

  MemberResponse? _detail;
  List<Widget> _navIndex = [];

  Future<void> _loadDetail() async {
    await RepoMember()
        .getMemberDetail()
        .then((res) => setState(() {
      _detail = res.data;

      _navIndex = [
        PageFeed(),
        PageCategory(),
        PageCreatePost(),
        const PageSearch(),
        PageMyBoard(),
      ];
    }));
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }



  int _selectedIndex = 0;

  void _onNavTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _navIndex.elementAt(_selectedIndex),
        bottomNavigationBar: Container(
          padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
          child: BottomNavigationBar(

            currentIndex: _selectedIndex,
            onTap: _onNavTapped,

            type: BottomNavigationBarType.shifting, // 바텀바 애니메이션 효과
            backgroundColor: Colors.white, // 배경색
            selectedItemColor: const Color.fromRGBO(0, 0, 0, 1), // 선택된 Item 색상
            unselectedItemColor: const Color.fromRGBO(0, 0, 0, 0.3), // 선택 되지 않은 Item 색상
            //showSelectedLabels: true, // 선택된 Item 의 label 노출 여부 - bool
            showUnselectedLabels: true, // 선택 되지 않은 Item 의 label 노출 여부 - bool
            items: const [
              BottomNavigationBarItem(icon: Icon(Icons.dynamic_feed_rounded),label: 'Feed'),
              BottomNavigationBarItem(icon: Icon(Icons.grid_3x3),label: 'Category'),
              BottomNavigationBarItem(icon: Icon(Icons.add),label: 'New'),
              BottomNavigationBarItem(icon: Icon(Icons.search_rounded), label: 'Search' ),
              BottomNavigationBarItem(icon: Icon(Icons.dashboard),label: 'My board'),
            ],

          ),

        )
    );
  }

}
