import 'package:dio/dio.dart';
import 'package:versus_app/model/recommend_board/recommend_board_list_result.dart';

import '../config/config_api.dart';
import '../functions/token_lib.dart';
import '../model/common_result.dart';

class RepoRecommendBoard {

  //유저가 추천한 게시글 모두 보기
  Future<RecommendBoardListResult> getRecommendBoards() async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();

    String baseUrl ='$apiUri/recommend-board/is-like/all/member-id/{memberId}';

    final response = await dio.get(
        baseUrl.replaceAll('{memberId}', token!),
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }
        )
    );

    return RecommendBoardListResult.fromJson(response.data);

  }

  //유저가 게시글 추천하기
  Future<CommonResult> setMemberRecommendBoard(num boardId) async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();

    String baseUrl ='$apiUri/recommend-board/new/member-id/$token/board-id/{boardId}/';

    final response = await dio.post(
      baseUrl.replaceAll('{boardId}', boardId.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (state) {
          return state == 200;
        }
      )
    );
    return CommonResult.fromJson(response.data);
  }

}