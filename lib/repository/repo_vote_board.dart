import 'package:dio/dio.dart';
import 'package:versus_app/config/config_api.dart';

import '../functions/token_lib.dart';
import '../model/common_result.dart';
import '../model/vote/vote_list_result.dart';

class RepoVoteBoard {

  // 유저가 투표한 게시글 전부 보기
  Future<VoteListResult> getVotes() async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();

    String baseUrl ='$apiUri/vote-board/member-id/{memberId}/all';

    final response = await dio.get(
      baseUrl.replaceAll('{memberId}', token!),
      options: Options(
        followRedirects: false,
        validateStatus: (state) {
          return state == 200;
        }
      )
    );

    return VoteListResult.fromJson(response.data);

  }

  // 유저 A 투표
  Future<CommonResult> setAVote(num boardId) async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();

    String baseUrl ='$apiUri/vote-board/new/a/member-id/$token/board-id/{boardId}';

    final response = await dio.post(
      baseUrl.replaceAll('{boardId}', boardId.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (state) {
          return state == 200;
        }
      )
    );
    return CommonResult.fromJson(response.data);
  }

  // 유저 B 투표
  Future<CommonResult> setBVote(num boardId) async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();

    String baseUrl ='$apiUri/vote-board/new/b/member-id/$token/board-id/{boardId}';

    final response = await dio.post(
        baseUrl.replaceAll('{boardId}', boardId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }
}