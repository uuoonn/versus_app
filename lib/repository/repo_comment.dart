import 'package:dio/dio.dart';
import 'package:versus_app/config/config_api.dart';
import 'package:versus_app/functions/token_lib.dart';
import 'package:versus_app/model/comments/comment_list_result.dart';
import 'package:versus_app/model/common_result.dart';

class RepoComment {

  //유저 댓글 달기
  Future<CommonResult> setUserComment(num voteBoardId) async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();

    String baseUrl = '$apiUri//v1/comment/new/member-id/$token/voteBoard-id/{voteBoardId}';

    final response = await dio.post(
      baseUrl.replaceAll('{voteBoardId}', voteBoardId.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (state) {
          return state == 200;
        }
      )
    );
    return CommonResult.fromJson(response.data);
  }

  //보드에 달린 게시글 보기 - 페이징처리

  Future<CommentListResult> getComments(num boardId, {int page = 1}) async {
    Dio dio = Dio();

    String baseUrl = '$apiUri/comment/board/vote-board/$boardId/{pageNum}';

    final response = await dio.get(
      baseUrl.replaceAll('{pageNum}', page.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (state) {
         return state == 200;
        }
      )
    );
    return CommentListResult.fromJson(response.data);
  }
}