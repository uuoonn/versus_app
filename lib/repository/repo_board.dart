import 'package:dio/dio.dart';
import 'package:versus_app/config/config_api.dart';
import 'package:versus_app/model/board/board_list_result.dart';
import 'package:versus_app/model/common_result.dart';
import '../functions/token_lib.dart';
import '../model/board/board_detail_result.dart';
import '../model/board/board_top_list_result.dart';
import '../model/enum/enum_category.dart';

class RepoBoard {

  // 보드 최신순 리스트로 보기
  Future<BoardListResult> getBoard() async {
    Dio dio = Dio();

    String baseUrl = '$apiUri/board/all';

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }));
    return BoardListResult.fromJson(response.data);
  }



  // 보드 최신순 리스트로 보기 - 페이징

  // {} => 선택값을 의미한다. 없다면 무조건 필수가 되는 것이다.
  Future<BoardListResult> getBoardPaging({int page = 1}) async {
    //Dio라는 전화기
    Dio dio =Dio();

    //replaceAll : 바꾸겠다. '{pageNum}'이 String을 page.toString()로 바꾸겠다.
    String baseUrl = '$apiUri/board/all/{pageNum}'.replaceAll('{pageNum}', page.toString());

    //get방식으로 전화를 걸면 기다려야함을 명시
    final response = await dio.get(
      //전화번호 명시
        baseUrl.replaceAll('{pageNum}', page.toString()),
        options: Options(
          //이 전화번호가 없으면 다른 곳으로 가니? 안간다.
            followRedirects: false,
            validateStatus: (status) {
              //연결 성공을 의미한다. 성공했을때만 아래의 return으로 이동한다.
              return status == 200;
            }));
    //Json주면서 줬던 모양 그래도 줄게(BoardListResult)
    return BoardListResult.fromJson(response.data);

  }



  // 게시글 상세보기
  Future<BoardDetailResult> getBoardDetail(num boardId) async {
    Dio dio = Dio();

    String baseUrl = '$apiUri/board/detail/board-id/{boardId}';

    final response = await dio.get(
        baseUrl.replaceAll('{boardId}', boardId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }));
    return BoardDetailResult.fromJson(response.data);
  }


  //유저가 쓴 게시글 리스트보기
  Future<BoardListResult> getUserPersonalBoard() async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();

    String baseUrl = '$apiUri/board/member-id/{memberId}/all';

    final response = await dio.get(
        baseUrl.replaceAll('{memberId}', token!),
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }));
    return BoardListResult.fromJson(response.data);
  }

  // 마이보드 해당 유저 글 최신순 리스트로 보기 - 페이징

  Future<BoardListResult> getPersonalBoardPaging({int page = 1}) async {
    String? token = await TokenLib.getMemberToken();
    Dio dio =Dio();

    String baseUrl = '$apiUri/board/member-id/$token/{pageNum}/all'.replaceAll('{pageNum}', page.toString());

    final response = await dio.get(
        baseUrl.replaceAll('{pageNum}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));
    return BoardListResult.fromJson(response.data);

  }

  // 게시글 추천수 10개이상 Top10 리스트 가져오기
  Future<BoardTopListResult> getLikeTop10() async {
    Dio dio = Dio();

    String baseUrl = '$apiUri/board/top10/all';

    final response = await dio.get(
      baseUrl,
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );
    return BoardTopListResult.fromJson(response.data);
  }

  // 게시글 카테고리별 리스트로 보기
  Future<BoardTopListResult> getCategoryBoard(EnumCategory category) async {

    Dio dio = Dio();

    String baseUrl = Uri.http('versuss.store:8080','v1/board/category/all',{'category':'${category.code}'}).toString();
    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }));
    return BoardTopListResult.fromJson(response.data);
  }

  //게시물 작성
  Future<CommonResult> setCreateBoard() async {
    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    String baseUrl ='$apiUri/board/new/member-id/$token';

    final response = await dio.post(
      baseUrl,
      options: Options(
        followRedirects: false,
        validateStatus: (state) {
          return state == 200;
        }
      )
    );
    return CommonResult.fromJson(response.data);
  }



}