import 'package:dio/dio.dart';
import 'package:versus_app/config/config_api.dart';
import 'package:versus_app/functions/token_lib.dart';
import 'package:versus_app/model/common_result.dart';
import 'package:versus_app/model/member/member_detail_result.dart';


class RepoMember {

  //멤버 정보 가져오기
  Future<MemberDetailResult> getMemberDetail() async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();

    String baseUrl = '$apiUri/member/detail/id/{id}';

    final response = await dio.get(
        baseUrl.replaceAll('{id}', token!),
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }));

    return MemberDetailResult.fromJson(response.data);
  }


  //멤버 등록
  Future<CommonResult> setMember() async {
    Dio dio = Dio();
    String baseUrl = '$apiUri/v1/member/join';

    final response = await dio.post(baseUrl,
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200 ;
        })
    );
    return CommonResult.fromJson(response.data);
  }
}