// 카테고리 페이지

import 'package:flutter/material.dart';
import 'package:versus_app/model/enum/enum_category.dart';
import '../../components/bar/component_app_bar_basic.dart';


class PageCategoryDetail extends StatefulWidget {
  const PageCategoryDetail({
    super.key,
    required this.categoryIndex,

  });

  //카테고리 인덱스 값 받아오기
  final int categoryIndex;

  @override
  State<PageCategoryDetail> createState() => _PageCategoryDailyState();
}

class _PageCategoryDailyState extends State<PageCategoryDetail> with SingleTickerProviderStateMixin {

  //탭바 컨트롤러
  late TabController _tabController;


  //초기화
  @override
  void initState() {
    super.initState();
    // TabController 초기화: 탭의 개수와 TickerProvider를 설정
    _tabController = TabController(length: EnumCategory.values.length, vsync: this);
    _tabController.index = widget.categoryIndex; // 전달된 인덱스 값으로 탭 위치 설정
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      // AppBar 설정: 커스텀 앱바 컴포넌트 사용
      appBar: ComponentAppBarBasic(
        text: 'Feed',
        onLeadingPressed: () {

          Navigator.of(context).pop();
        },
      ),

      // DefaultTabController로 TabBar 및 TabBarView 설정
      body: DefaultTabController(
        length: EnumCategory.values.length, // 카테고리 리스트 Enum 값 길이 가져오기
        child: Column(
          children: [
            TabBar(

              // TabController 연결
              controller: _tabController,

              //map 함수는 리스트의 요소를 하나씩 전달한 결과로
              //lterable 객체를 생성하기 때문에 toList 함수로 변환한다.
              tabs: EnumCategory.values.map((e) {
                return Tab(text: e.name);
              }).toList(),

              // 탭 수가 많으면 자동 스크롤
              isScrollable: true,

              // 인디케이터 컬러
              indicatorColor: const Color.fromRGBO(0, 0, 0, 1),

              // 인디케이터 사이즈 탭 사이즈로 맞춤 ( 라벨 사이즈는 label 사용하기)
              indicatorSize: TabBarIndicatorSize.tab,

              // 인디케이터 두께 설정
              indicatorWeight: 2.0,

              // 선택된 탭 글자 색상
              labelColor: const Color.fromRGBO(0, 0, 0, 1),

              // 선택 되지 않은 탭 글자 색상
              unselectedLabelColor: const Color.fromRGBO(
                  0, 0, 0, 0.5),

              //탭바 정렬 추가
              tabAlignment: TabAlignment.start,
            ),

            Expanded(
              //실질적으로 바디부분에 보여지는 내용
              child: TabBarView(
                controller: _tabController, // TabController 연결
                children: [
                  Center(child: Text('일상 컨텐츠')),
                  Center(child: Text('유머 컨텐츠')),
                  Center(child: Text('정보 컨텐츠')),
                  Center(child: Text('연예 컨텐츠')),
                  Center(child: Text('게임 컨텐츠')),
                  Center(child: Text('만화 컨텐츠')),
                  Center(child: Text('스포츠 컨텐츠')),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

}
