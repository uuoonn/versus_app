// 카테고리 페이지

import 'package:flutter/material.dart';
import 'package:versus_app/components/board/component_preview_board_item.dart';
import 'package:versus_app/model/board/board_item.dart';
import 'package:versus_app/model/enum/enum_category.dart';
import 'package:versus_app/repository/repo_board.dart';
import '../../components/bar/component_app_bar_basic.dart';
import '../board/page_board_detail.dart';


class PageCategoryTab extends StatefulWidget {
  const PageCategoryTab({
    super.key,
    required this.categoryIndex,
  });

  //카테고리 인덱스 값 받아오기
  final int categoryIndex;

  @override
  State<PageCategoryTab> createState() => _PageCategoryTabState();
}

class _PageCategoryTabState extends State<PageCategoryTab> with SingleTickerProviderStateMixin {
  List<BoardItem> _list = [];

  //탭바 컨트롤러
  late TabController _tabController;

  Future<void> _loadList({bool reFresh = false}) async {
    // 데이터를 로딩하는 동안 리스트 비워두기
    setState(() {
      _list = [];
    });
    // 데이터 받아오기
    await RepoBoard()
        .getCategoryBoard(EnumCategory.values[_tabController.index])
        .then((res) => {
      setState(() {
        _list = res.list;
      })
    });
  }

  //초기화
  @override
  void initState() {
    super.initState();

    // TabController 초기화: 탭의 개수와 TickerProvider를 설정
    _tabController = TabController(length: EnumCategory.values.length, vsync: this);
    _tabController.index = widget.categoryIndex; // 전달된 인덱스 값으로 탭 위치 설정

    _loadList();
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      // AppBar 설정: 커스텀 앱바 컴포넌트 사용
      appBar: ComponentAppBarBasic(
        text: 'Feed',
        onLeadingPressed: () {

          Navigator.of(context).pop();
        },
      ),

      // DefaultTabController로 TabBar 및 TabBarView 설정
      body: DefaultTabController(
        length: EnumCategory.values.length, // 카테고리 리스트 Enum 값 길이 가져오기
        child: Column(
          children: [
            TabBar(

              // TabController 연결
              controller: _tabController,

              //map 함수는 리스트의 요소를 하나씩 전달한 결과로
              //lterable 객체를 생성하기 때문에 toList 함수로 변환한다.
              tabs: EnumCategory.values.map((e) {
                return Tab(text: e.name);
              }).toList(),

              onTap: (index) {
                  _loadList(reFresh: true);
              },


              // 탭 수가 많으면 자동 스크롤
              isScrollable: true,

              // 인디케이터 컬러
              indicatorColor: const Color.fromRGBO(0, 0, 0, 1),

              // 인디케이터 사이즈 탭 사이즈로 맞춤 ( 라벨 사이즈는 label 사용하기)
              indicatorSize: TabBarIndicatorSize.tab,

              // 인디케이터 두께 설정
              indicatorWeight: 2.0,

              // 선택된 탭 글자 색상
              labelColor: const Color.fromRGBO(0, 0, 0, 1),

              // 선택 되지 않은 탭 글자 색상
              unselectedLabelColor: const Color.fromRGBO(
                  0, 0, 0, 0.5),

              //탭바 정렬 추가
              tabAlignment: TabAlignment.start,
            ),

              if(_list.isNotEmpty)
              Expanded(
                //실질적으로 바디부분에 보여지는 내용
                child: TabBarView(
                  controller: _tabController, // TabController 연결
                  children: List.generate(
                    EnumCategory.values.length,
                        (index) => _buildTabView(index),
                  ),
                ),
              ),
            if(_list.isEmpty)
              CircularProgressIndicator()

          ],
        ),
      ),
    );
  }

  Widget _buildTabView(int index) {
      return Center(
        child: ListView.builder(
          itemCount: _list.length,
          itemBuilder: (BuildContext context, int idx) {
            return ComponentPreviewBoardItem(
              boardItem: _list[idx],
              callback: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => PageBoardDetail(id: _list[idx].id),
                ));
              },
            );
          },
        ),
      );
  }
}