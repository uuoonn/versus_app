// 인트로 화면
// ++ 로딩 중 애니메이션 추가하기

import 'package:flutter/material.dart';
import 'package:versus_app/config/path.dart';

class PageIntroScreen extends StatefulWidget {
  const PageIntroScreen({super.key});

  @override
  State<PageIntroScreen> createState() => _PageSplashScreenState();
}

class _PageSplashScreenState extends State<PageIntroScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: Image.asset('${pathLogo}logo_white.png'),
      ),
    );
  }
}
