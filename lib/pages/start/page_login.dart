//로그인 페이지 (아이디 비밀번호 입력 및 회원 가입 버튼)

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:versus_app/components/form_field/component_login_text_field.dart';
import 'package:versus_app/config/path.dart';
import 'package:versus_app/pages/start/page_join_member.dart';
import 'package:versus_app/config/size.dart';

import '../../functions/token_lib.dart';
import '../../middleware/middleware_login_check.dart';
import '../../model/login/login_request.dart';
import '../../repository/repo_login.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  State<PageLogin> createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLogin(LoginRequest loginRequest) async {
    await RepoLogin().doLogin(loginRequest).then((res) {
      // api에서 받아온 결과값을 token에 넣는다.
      TokenLib.setMemberToken(res.data.token);
      TokenLib.setMemberName(res.data.name);
      // 미들웨어에게 부탁해서 토큰값 여부 검사 후 페이지 이동을 부탁한다.
      MiddlewareLoginCheck().check(context);
    }).catchError((err) {
      //아이디 비밀번호 틀릴 경우 스낵바
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('아이디와 비밀번호를 확인해주세요.'),
        backgroundColor: Colors.red,
      ));
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
    body: Padding(
      padding: const EdgeInsets.all(16.0),
      child: ListView(
        children: [
          const SizedBox(height: largeGap,),
          SizedBox(
            width: 250,
            height: 250,
            child: Image.asset(
                '${pathLogo}logo_black.png',
            ),
          ),
          const SizedBox(height: mediumGap,),
          FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              children: [
                //아이디 입력폼
                const ComponentLoginTextField(title: 'id', hintText: '아이디를 입력해주세요', width: 300,obscureText: false, name: 'username'),
                const SizedBox(height: mediumGap,),
                //비밀번호 입력폼
                const ComponentLoginTextField(title: 'password', hintText: '비밀번호를 입력해주세요.', width: 300, obscureText: true, name: 'password'),

                const SizedBox(height: largeGap,),

                //로그인 버튼
                TextButton(
                  child: const Text('로그인'),
                  onPressed: () {
                  if(_formKey.currentState!.saveAndValidate()) {
                    LoginRequest loginRequest = LoginRequest(
                      _formKey.currentState!.fields['username']!.value,
                      _formKey.currentState!.fields['password']!.value,
                    );
                    _doLogin(loginRequest);
                  }
                },

                ),
                const SizedBox(height: smallGap,),

                //회원가입 버튼
                TextButton(
                  child: const Text('회원가입'),
                    onPressed: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageJoinMember()));
                    },
                ),
              ],
            ),
          )
        ],
      ),
    ),
    );
  }
}
