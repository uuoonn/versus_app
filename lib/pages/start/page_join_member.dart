// 회원 가입 페이지

import 'package:flutter/material.dart';
import 'package:versus_app/components/form_field/component_drop_box.dart';
import 'package:versus_app/pages/start/page_login.dart';
import '../../components/form_field/component_text_form.dart';
import '../../config/size.dart';

class PageJoinMember extends StatefulWidget {
  const PageJoinMember({super.key});

  @override
  State<PageJoinMember> createState() => _PageJoinMemberState();
}

class _PageJoinMemberState extends State<PageJoinMember> {

  List<String> genderList = ['남자', '여자',];
  String? selectedValue;


  @override
  Widget build(BuildContext context) {

    double screenWidth = MediaQuery.of(context).size.width;
    double paddingValue = (screenWidth - 300.0) / 2.0; // 컨텐츠 내용 width 300으로 맞춤

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          '회원가입',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: paddingValue),
            color: const Color.fromRGBO(255, 255, 255, 0.6),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const ComponentTextForm(title: '이름', width: 260, obscureText: false, height: 46, hintText: '', lines: 1,),

                const SizedBox(height: mediumGap,),

                const ComponentTextForm(title: '휴대번호', width: 260, height: 46, obscureText: false, hintText: '010-0000-0000', lines: 1,),

                const SizedBox(height: mediumGap,),

                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    const ComponentTextForm(title: '아이디', width: 150, obscureText: false, height: 46, hintText: '', lines: 1,),
                    const SizedBox(width: mediumGap,),
                    SizedBox(
                      width: 100,
                      height: 46,
                      child: TextButton(
                        onPressed: (){},
                        style: TextButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(circularRadiusBase)
                          )
                        ),
                        child: const Text('중복확인'),
                      ),
                    ),
                  ],
                ),

                const SizedBox(height: mediumGap,),

                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,

                  children: [
                    const ComponentTextForm(title: '닉네임', width: 150, obscureText: false, height: 46, hintText: '', lines: 1,),
                    const SizedBox(width: mediumGap,),
                    SizedBox(
                      width: 100,
                      height: 46,
                      child: TextButton(
                          onPressed: (){},
                          style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)
                            )
                        ),
                          child: const Text('중복확인'),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: mediumGap,),

                const ComponentTextForm(title: '비밀번호', obscureText: true, width: 260, height: 46, hintText: '비밀번호를 입력해주세요.', lines: 1,),

                const SizedBox(height: mediumGap,),

                const ComponentTextForm(title: '비밀번호 확인', obscureText: true, width: 260, height: 46, hintText: '비밀번호를 한번 더 입력해주세요.', lines: 1,),

                const SizedBox(height: mediumGap,),

                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      '성별',
                      style: TextStyle(
                        fontSize: fontSizeSmall,
                      ),
                    ),
                    const SizedBox(height: smallGap,),
                    SizedBox(
                      height: 55,
                      width: 150,
                      child: DropdownButtonFormField(
                        isExpanded: true,
                        borderRadius: BorderRadius.circular(circularRadiusBase),
                        decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(vertical: 20),
                            border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(circularRadiusBase),
                          )
                        ),
                        hint: Container(
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: const Text (
                            '성별을 선택하세요',
                            style: TextStyle(fontSize: fontSizeSmall),
                          ),
                        ),
                        items: genderList.map((String item) {
                          return DropdownMenuItem<String>(
                            value: item,
                            child: Text(
                                item,
                              style: const TextStyle(
                                fontSize: fontSizeSmall,
                              ),
                            ),
                          );
                        }).toList(),
                        validator: (value) {
                          if (value == null) {
                            return '성별을 선택해 주세요.';
                          }
                          return null;
                        },
                        onChanged: (dynamic value) {},
                        onSaved: (value){
                          selectedValue = value.toString();
                        },

                      ),
                    ),
                  ],
                ),
                const SizedBox(height: mediumGap,),

                const ComponentTextForm(title: '이메일', width: 260, height: 46, obscureText: false, hintText: 'example@example.com', lines: 1,),

                const SizedBox(height: mediumGap,),

                const ComponentTextForm(title: '생년월일', width: 260, height: 46, obscureText: false, hintText: '2000-01-01', lines: 1,),

                const SizedBox(height: largeGap,),

                TextButton(
                    onPressed: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageLogin()));
                    },
                    child: const Text(
                      '회원가입하기'
                    ),
                ),
                const SizedBox(height: largeGap,)
              ],
            ),
        ),
        ),
      );
  }
}
