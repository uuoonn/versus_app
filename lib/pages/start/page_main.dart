//메인 페이지 (로그인 및 회원 가입 선택 페이지)

import 'package:flutter/material.dart';
import 'package:versus_app/config/path.dart';
import 'package:versus_app/pages/start/page_join_member.dart';
import 'package:versus_app/pages/start/page_login.dart';

import '../../config/size.dart';

class PageMain extends StatelessWidget {
  const PageMain({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body : Stack(
        children: [
          Column(
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageLogin()));
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height/2,
                  alignment: Alignment.topCenter,
                  // color: const Color.fromRGBO(255, 255, 255, 1.0),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width-50,
                        height: MediaQuery.of(context).size.height/2-130,
                        decoration: BoxDecoration(
                          border: Border.all(
                              width: 2,
                              color: const Color.fromRGBO(0, 0, 0, 0.8)
                          ),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width/1.22,
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(10),
                        child: const Text(
                          '로그인',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: fontSizeXlarge2,
                            color: Color.fromRGBO(0, 0, 0, 1.0),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageJoinMember()));
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height/2,
                  alignment: Alignment.bottomCenter,
                  // color: const Color.fromRGBO(255, 255, 255, 1.0),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width-50,
                        height: MediaQuery.of(context).size.height/2-130,
                        decoration:
                        BoxDecoration(
                            border: Border.all(
                                width: 2,
                                color: const Color.fromRGBO(0, 0, 0, 0.8)
                            ),
                            borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width/1.22,
                        padding: EdgeInsets.all(10),
                        alignment: Alignment.center,
                        child: const Text(
                          '회원가입',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: fontSizeXlarge2,
                            color: Color.fromRGBO(0, 0, 0, 0.8),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            alignment: Alignment.center,
            child: Image.asset('${pathLogo}logo_black.png',
            width: 150,
            height: 150,)
          ),

        ],
      ),
    );
  }
}
