//보드 상세보기 페이지 (게시물 조회)

import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:speech_balloon/speech_balloon.dart';
import 'package:versus_app/config/size.dart';
import 'package:versus_app/model/board/board_response.dart';
import 'package:versus_app/model/comments/comment_vote_board_item.dart';
import 'package:versus_app/repository/repo_board.dart';
import 'package:versus_app/repository/repo_recommend_board.dart';
import 'package:versus_app/repository/repo_vote_board.dart';

import '../../components/bar/component_search_bar.dart';
import '../../components/board/component_choice_a_box.dart';
import '../../components/board/component_choice_b_box.dart';
import '../../components/component_chat_bubble.dart';
import '../../config/path.dart';



class PageBoardDetail extends StatefulWidget {
  const PageBoardDetail({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<PageBoardDetail> createState() => _PageBoardDetailState();
}

class _PageBoardDetailState extends State<PageBoardDetail> {
  BoardResponse? detail;
  List<CommentVoteBoardItem> commentsList = [];

  //추천버튼
  bool _isLiked = false;

  //투표여부 확인
  bool _isVoted = false;

  bool _isLikedComment = false;
  // //댓글
  // CommentVoteBoardItem? comments;

  // //투표 선택
  // bool _isA = false;



  // API 보드 상세보기 불러오기
  Future<void> _loadDetail() async {
    await RepoBoard().getBoardDetail(widget.id)
        .then((res) => {
      setState(() {
        detail = res.data;
      })
    });
  }

  // API A 투표
  Future<void> _choiceA() async {
    await RepoVoteBoard().setAVote(widget.id)
        .then((res) => {
          setState(() {
            _loadDetail();
            _isVoted = true;
    })});
  }

  // API B 투표
  Future<void> _choiceB() async {
    await RepoVoteBoard().setBVote(widget.id)
        .then((res) => {
      setState(() {
        _loadDetail();
        _isVoted = true;
      })});
  }

  //API 유저가 게시글 추천하기
  Future<void> _userRecommendBoard() async {
    await RepoRecommendBoard().setMemberRecommendBoard(widget.id)
        .then((res) => {
          setState(() {
            _isLiked =!_isLiked;
            if(_isLiked) {
              detail!.likeCount++;
            } else {
              detail!.likeCount--;
            }
          })
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
        actions: [Icon(Icons.more_vert)],
      ),
      extendBodyBehindAppBar: true,
      body : _buildBody(context)
    );
  }
  Widget _buildBody(BuildContext context) {

    // _detail이 null이거나 _detail!.id가 null인지 확인
    //상세 정보가 로딩이 되지않았거나, id가 null인 경우 확인
    if (detail == null || detail!.id == null) {
      //만약 true인 경우 로딩중임을 알리는 CircularProgressIndicator 표시
      return const Center(child: CircularProgressIndicator());
    } else {
      //false인 경우,
      //_detail!.id가 null인 경우 빈 문자열('')을 사용 => null을 대체한다.
      final id = detail!.id ?? '';
      //실행
      return Stack(
        children: [
          Container(
            child: Column(
              children: [
                Hero(
                  tag: 'choiceA_$id',
                  child: ComponentChoiceABox(boardResponse: detail!, callback: () {
                    _choiceA();
                  }),
                ),
                Hero(
                  tag: 'choiceB_$id',
                  child: ComponentChoiceBBox(boardResponse: detail!, callback: () {
                    _choiceB();
                  }),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.center,
            child: Image.asset('${pathLogo}logo_black.png',
              width: 150,
              height: 150,
            ),
          ),
          Container(
            padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        const Text('작성자',style: TextStyle(fontSize: fontSizeSmall),),
                        Text(detail!.memberNickName,style: const TextStyle(fontSize: fontSizeSmall,fontWeight: FontWeight.bold),),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          //하단 추천 , 댓글 아이콘
          Container(
            padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          //좋아요 아이콘, 좋아요 수
                          children: [
                            Container(
                              child: IconButton(
                                iconSize: 25,
                                icon: _isLiked
                                    ? const Icon(Icons.thumb_up, color: Color.fromRGBO(255, 215, 0, 1.0),)
                                    : const Icon(Icons.thumb_up_outlined, color: Color.fromRGBO(0, 0, 0, 0.5),),
                                onPressed: () {
                                  _userRecommendBoard();
                                },
                              ),
                            ),
                            Text('${detail!.likeCount}',style: const TextStyle(fontSize: fontSizeSmall,color: Color.fromRGBO(0, 0, 0, 0.5) ),)
                          ],
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        //댓글 아이콘, 댓글 수
                        // Text('댓글수',style: const TextStyle(fontSize: fontSizeMicro,color: Color.fromRGBO(0, 0, 0, 0.5) ),),
                        Container(
                          child: IconButton(
                            iconSize: 25,
                            icon: const Icon(Icons.mode_comment_outlined, color: Color.fromRGBO(0, 0, 0, 0.5),),
                            onPressed: () {
                              _showCommentSheet(context);
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      );
    }
  }
  void _showCommentSheet(BuildContext context) {
      // 사용자가 투표한 경우에만 댓글 입력 창을 표시
      showModalBottomSheet(
        backgroundColor: Colors.white,
        context: context,
        isScrollControlled: true,
        builder: (BuildContext context) {
          return SingleChildScrollView(
              child: Container(
                height: MediaQuery.of(context).size.height,
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text('댓글',style: TextStyle(fontSize: fontSizeLarge),),
                        IconButton(onPressed: () => Navigator.of(context).pop(),
                            icon: Icon(Icons.close)),
                      ],
                    ),
                    LinearPercentIndicator(
                      progressColor:Color.fromRGBO(255, 205, 210, 1),
                      backgroundColor: Color.fromRGBO(144, 202, 249, 1),
                      lineHeight: 20,
                      percent: 0.3,
                      padding: EdgeInsets.zero,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('2표'),
                        Text('8표')
                    ],),
                    const SizedBox(height: mediumGap,),
                    ComponentSearchBar(),
                    const SizedBox(height: mediumGap,),
                    const SizedBox(height: mediumGap,),
                    Expanded(
                      child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const CircleAvatar(
                                  backgroundColor: Colors.white,
                                  child : Text('강민둥',style: TextStyle(fontSize: fontSizeMicro),)
                              ),
                              SizedBox(width: mediumGap,),
                              const SpeechBalloon(
                                  nipLocation: NipLocation.left,
                                  color: Color.fromRGBO(255, 205, 210, 1),
                                  height: 60,
                                  width: 150,
                                  borderRadius: 10,
                                  child: Row(
                                    children: [
                                      SizedBox(width: mediumGap,),
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: [
                                          Text('차 너무 막혀서 \n당연히 대중교통이지'),
                                          Text('1분 전',style: TextStyle(fontSize: fontSizeMicro),)
                                        ],
                                      ),
                              ],)),
                              IconButton(
                                iconSize: 20,
                                icon: _isLikedComment
                                    ? Icon(Icons.favorite, color: Color.fromRGBO(255, 205, 210, 1),)
                                    : Icon(Icons.favorite_border, color: Color.fromRGBO(255, 205, 210, 1),),
                                onPressed: () {
                                  setState(() {
                                    _isLikedComment = !_isLikedComment;
                                  });
                                },
                              ),
                            ],
                          ),
                          SizedBox(height: mediumGap,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              IconButton(
                                iconSize: 20,
                                icon: _isLikedComment
                                    ?  Icon(Icons.favorite, color: Color.fromRGBO(144, 202, 249, 1),)
                                    :  Icon(Icons.favorite_border, color: Color.fromRGBO(144, 202, 249, 1),),
                                onPressed: () {
                                  setState(() {
                                    _isLikedComment = !_isLikedComment;
                                  });
                                },
                              ),
                              SizedBox(width: smallGap,),
                              const SpeechBalloon(
                                  nipLocation: NipLocation.right,
                                  color: Color.fromRGBO(144, 202, 249, 1),
                                  height: 60,
                                  width: 125,
                                  borderRadius: 10,
                                  child: Row(
                                    children: [
                                      SizedBox(width: mediumGap,),
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('지옥철 너무 끔찍'),
                                          Text('1분 전',style: TextStyle(fontSize: fontSizeMicro),)
                                        ],
                                      ),
                                    ],)),
                              SizedBox(width: mediumGap,),
                              const CircleAvatar(
                                  backgroundColor: Colors.white,
                                  child : Text('파란알약',style: TextStyle(fontSize: fontSizeMicro),)
                              ),
                            ],
                          ),

                          const SizedBox(height: mediumGap,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              IconButton(
                                iconSize: 20,
                                icon: _isLikedComment
                                    ?  Icon(Icons.favorite, color: Color.fromRGBO(144, 202, 249, 1),)
                                    :  Icon(Icons.favorite_border, color: Color.fromRGBO(144, 202, 249, 1),),
                                onPressed: () {
                                  setState(() {
                                    _isLikedComment = !_isLikedComment;
                                  });
                                },
                              ),
                              const SizedBox(width: smallGap,),
                              const SpeechBalloon(
                                  nipLocation: NipLocation.right,
                                  color: Color.fromRGBO(144, 202, 249, 1),
                                  height: 60,
                                  width: 190,
                                  borderRadius: 10,
                                  child: Row(
                                    children: [
                                      SizedBox(width: mediumGap,),
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('낑겨가느니 내 차타고 간다'),
                                          Text('2분 전',style: TextStyle(fontSize: fontSizeMicro),)
                                        ],
                                      ),
                                    ],)),
                              SizedBox(width: mediumGap,),
                              const CircleAvatar(
                                  backgroundColor: Colors.white,
                                  child : Text('기범스',style: TextStyle(fontSize: fontSizeMicro),)
                              ),
                            ],
                          ),
                          const SizedBox(height: mediumGap,),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const CircleAvatar(
                                  backgroundColor: Colors.white,
                                  child : Text('박진순',style: TextStyle(fontSize: fontSizeMicro),)
                              ),
                              SizedBox(width: mediumGap,),
                              const SpeechBalloon(
                                  nipLocation: NipLocation.left,
                                  color: Color.fromRGBO(255, 205, 210, 1),
                                  height: 60,
                                  width: 210,
                                  borderRadius: 10,
                                  child: Row(
                                    children: [
                                      SizedBox(width: mediumGap,),
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: [
                                          Text('차비도 아끼고 \n환경도 지키고 대중교통이 좋지'),
                                          Text('5분 전',style: TextStyle(fontSize: fontSizeMicro),)
                                        ],
                                      ),
                                    ],)),
                              IconButton(
                                iconSize: 20,
                                icon: _isLikedComment
                                    ? Icon(Icons.favorite, color: Color.fromRGBO(255, 205, 210, 1),)
                                    : Icon(Icons.favorite_border, color: Color.fromRGBO(255, 205, 210, 1),),
                                onPressed: () {
                                  setState(() {
                                    _isLikedComment = !_isLikedComment;
                                  });
                                },
                              ),
                              SizedBox(width: smallGap,),
                            ],
                          ),
                          const SizedBox(height: mediumGap,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              IconButton(
                                iconSize: 20,
                                icon: _isLikedComment
                                    ?  Icon(Icons.favorite, color: Color.fromRGBO(144, 202, 249, 1),)
                                    :  Icon(Icons.favorite_border, color: Color.fromRGBO(144, 202, 249, 1),),
                                onPressed: () {
                                  setState(() {
                                    _isLikedComment = !_isLikedComment;
                                  });
                                },
                              ),
                              const SizedBox(width: smallGap,),
                              const SpeechBalloon(
                                  nipLocation: NipLocation.right,
                                  color: Color.fromRGBO(144, 202, 249, 1),
                                  height: 60,
                                  width: 110,
                                  borderRadius: 10,
                                  child: Row(
                                    children: [
                                      SizedBox(width: mediumGap,),
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('내 차가 개꿀'),
                                          Text('7분 전',style: TextStyle(fontSize: fontSizeMicro),)
                                        ],
                                      ),
                                    ],)),
                              SizedBox(width: mediumGap,),
                              const CircleAvatar(
                                  backgroundColor: Colors.white,
                                  child : Text('최재사',style: TextStyle(fontSize: fontSizeMicro),)
                              ),
                            ],
                          ),
                          const SizedBox(height: mediumGap,),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const CircleAvatar(
                                  backgroundColor: Colors.white,
                                  child : Text('홍안경',style: TextStyle(fontSize: fontSizeMicro),)
                              ),
                              SizedBox(width: mediumGap,),
                              const SpeechBalloon(
                                  nipLocation: NipLocation.left,
                                  color: Color.fromRGBO(255, 205, 210, 1),
                                  height: 60,
                                  width: 200,
                                  borderRadius: 10,
                                  child: Row(
                                    children: [
                                      SizedBox(width: mediumGap,),
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: [
                                          Text('시간 맞춰 오는 지하철이 좋아'),
                                          Text('10분 전',style: TextStyle(fontSize: fontSizeMicro),)
                                        ],
                                      ),
                                    ],)),
                              IconButton(
                                iconSize: 20,
                                icon: _isLikedComment
                                    ? Icon(Icons.favorite, color: Color.fromRGBO(255, 205, 210, 1),)
                                    : Icon(Icons.favorite_border, color: Color.fromRGBO(255, 205, 210, 1),),
                                onPressed: () {
                                  setState(() {
                                    _isLikedComment = !_isLikedComment;
                                  });
                                },
                              ),
                              SizedBox(width: smallGap,),
                            ],
                          ),
                          SizedBox(height: largeGap,),

                        ],
                      )
                    )
                  ],

                ),
              ));
        },
      );
  }



}

