// import 'package:flutter/material.dart';
// import 'package:versus_app/components/bar/component_app_bar_basic.dart';
// import 'package:versus_app/model/comments/comment_vote_board_item.dart';
// import 'package:versus_app/repository/repo_comment.dart';
//
// import '../../model/member/member_response.dart';
// import '../../repository/repo_member.dart';
//
// class PageBoardComments extends StatefulWidget {
//   const PageBoardComments({super.key, required this.id});
//   final num id;
//
//   @override
//   State<PageBoardComments> createState() => _PageBoardCommentsState();
// }
//
// class _PageBoardCommentsState extends State<PageBoardComments> {
//   //멤버 세부 정보
//   MemberResponse? detail;
//
//   CommentVoteBoardItem? commentVoteBoardItem;
//
//
//   List<CommentVoteBoardItem> _list = [];
//   int _totalItemCount = 0;
//   int _totalPage = 1;
//   int _currentPage = 1;
//
//   //스크롤 컨트롤러
//   final _scrollerController = ScrollController();
//
//   //API에서 멤버 정보 불러오기
//   Future<void> _loadDetail() async {
//     await RepoMember().getMemberDetail()
//         .then((res) => {
//       setState(() {
//         detail = res.data;
//       })
//     });
//   }
//
//   //API 유저 댓글 달기
//   Future<void> _userCommentCreate() async {
//     await RepoComment().setUserComment(widget.id)
//         .then((res) => {
//       // setState(() {
//       //   _detail = res.data;
//       // })
//     });
//   }
//
//   //새로 고침 할거니 ? 언급안하면 false이다.
//   Future<void> _loadItems({bool reFresh = false}) async {
//     //새로고침을 할 거라면,
//     if (reFresh) {
//       setState(() { //원웨이바인딩이기때문에 바뀌게 되면 계속 알려줘야한다. = setState
//         _list = []; // 리스트 싹 날리고 (초기화)
//         _totalItemCount = 0; //
//         _totalPage = 1; // 총 페이지 1로 초기화
//         _currentPage = 1; // 현재 페이지 1로 초기화
//       });
//     }
//     //만약에 .. 현재 페이지가 총 페이지보다 작거나 같을 때
//     if(_currentPage <= _totalPage) {
//       await RepoComment().getComments(page: _currentPage)
//           .then((res) {
//         setState(() {
//           _totalPage = res.totalPage.toInt();
//           _totalItemCount = res.totalCount.toInt();
//           _list = [..._list, ...res.list];
//
//           _currentPage++;
//         });
//       }).catchError((err) {
//         debugPrint(err);
//       });
//     }
//
//     if (reFresh) {
//       _scrollerController.animateTo(
//           0,
//           duration: const Duration(microseconds: 3000),
//           curve: Curves.easeOut);
//     }
//   }
//
//   //초기화
//   @override
//   void initState() {
//     super.initState();
//     _loadDetail();
//
//     //addListener : 계속 주시하겠다. 감시자 역할
//     _scrollerController.addListener(() {
//       //만약에.. 스크롤 설정값(offset) = 현재 스크롤 위치가 최대일 경우, _loadItems 실행
//       if(_scrollerController.offset == _scrollerController.position.maxScrollExtent) {
//         _loadItems();
//       }
//     });
//
//     //탄생시 한번만 실행되는 것
//     _loadItems(reFresh: true);
//     _userCommentCreate();
//
//   }
//
//
//   @override
//   Widget build(BuildContext context) {
//
//     double screenWidth = MediaQuery.of(context).size.width;
//     double paddingValue = (screenWidth - 320.0) / 2.0;
//
//     return Scaffold(
//       appBar: ComponentAppBarBasic(
//         text: 'Comments',
//         onLeadingPressed: () {},
//       ),
//       body: Column(
//         children: [
//           Text('${commentVoteBoardItem.}')
//         ],
//       ),
//     );
//   }
// }
