// 문의글 등록 페이지

import 'package:flutter/material.dart';
import 'package:versus_app/components/form_field/component_text_form.dart';
import 'package:versus_app/components/pop_up/component_pop_up_alert.dart';
import 'package:versus_app/config/size.dart';

class PageMemberAskCreate extends StatefulWidget {
  const PageMemberAskCreate({super.key});

  @override
  State<PageMemberAskCreate> createState() => _PageMemberAskCreateState();
}

class _PageMemberAskCreateState extends State<PageMemberAskCreate> {
  @override
  Widget build(BuildContext context) {

    double screenWidth = MediaQuery.of(context).size.width;
    double paddingValue = (screenWidth - 300.0) / 2.0; // 컨텐츠 내용 width 300으로 맞춤

    return Scaffold(
      appBar: AppBar(
        title:
        const Text(
          '1:1 문의하기',
          style: TextStyle(
            fontSize: fontSizeXlarge,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: const Color.fromRGBO(0, 0, 0, 0),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: paddingValue),
        child: Column(
          children: [

            const SizedBox(height: mediumGap,),

            const ComponentTextForm(title: '문의 제목', width: 300, height: 46, obscureText: false, hintText: '제목을 작성해주세요.', lines: 1,),

            const SizedBox(height: largeGap,),

            const ComponentTextForm(title: '문의 내용', width: 300, height: 300, obscureText: false, hintText: '내용을 입력해주세요.', lines: 10),

            const SizedBox(height: largeGap,),

            SizedBox(
              width: 100,
              height: 46,
              child: TextButton(
                onPressed: (){
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return ComponentPopUpAlert(title: '문의글이 등록되었습니다.', contents: '', text: '닫기', callback: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageMemberAskCreate()));
                      });
                    },
                  );
                },
                style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)
                    )
                ),
                child: const Text('보내기'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
