import 'package:flutter/material.dart';
import '../../config/size.dart';

class PageNotification extends StatefulWidget {
  const PageNotification({Key? key}) : super(key: key);

  @override
  State<PageNotification> createState() => _PageNotificationState();
}

class _PageNotificationState extends State<PageNotification> {
  bool _isCheckedAll = false;
  bool _isChecked1 = false;
  bool _isChecked2 = false;
  bool _isChecked3 = false;
  bool _isChecked4 = false;

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double paddingValue = (screenWidth - 300.0) / 2.0; // 컨텐츠 내용 width 300으로 맞춤

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          '알림 설정',
          style: TextStyle(
            fontSize: fontSizeXlarge,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: const Color.fromRGBO(0, 0, 0, 0),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: paddingValue),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.all(5),
              alignment: Alignment.topLeft,
              child: const Text(
                '전체 알림 설정',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: fontSizeLarge,
                ),
              ),
            ),
            SizedBox(
              height: 50,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 10),
                    child: const Text(
                      '전체 알림',
                      style: TextStyle(
                        fontSize: fontSizeLarge,
                      ),
                    ),
                  ),
                  Switch(
                    activeColor: Color.fromRGBO(0, 0, 0, 0.05),
                    value: _isCheckedAll,
                    onChanged: (value) { // 체크하게 되면
                      setState(() {
                        _isCheckedAll = value;
                        _isChecked1 = value;
                        _isChecked2 = value;
                        _isChecked3 = value;
                        _isChecked4 = value;// 체크한 값(true)로 변경
                      });
                    },
                  ),
                ],
              ),
            ),
            const Divider(thickness: 1,),
            Container(
              margin: const EdgeInsets.all(5),
              alignment: Alignment.topLeft,
              child: const Text(
                '마이보드 알림',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: fontSizeLarge,
                ),
              ),

            ),
            SizedBox(
              height: 50,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 10),
                    child: const Text(
                      '내 게시글 추천 알림',
                      style: TextStyle(
                        fontSize: fontSizeLarge,
                      ),
                    ),
                  ),
                  Switch(
                    activeColor: Color.fromRGBO(0, 0, 0, 0.05),
                    value: _isChecked1,
                    onChanged: (value) { // 체크하게 되면
                      setState(() {
                        _isChecked1 = value; // 체크한 값(true)로 변경
                       });
                    },
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 50,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 10),
                    child: const Text(
                      '내 게시글 댓글 알림',
                      style: TextStyle(
                        fontSize: fontSizeLarge,
                      ),
                    ),
                  ),
                  Switch(
                    activeColor: Color.fromRGBO(0, 0, 0, 0.05),
                    value: _isChecked2,
                    onChanged: (value) {
                      setState(() {
                        _isChecked2 = value;
                      });
                    },
                  ),
                ],
              ),
            ),
            const Divider(thickness: 1,),
            Container(
              margin: const EdgeInsets.all(5),
              alignment: Alignment.topLeft,
              child: const Text(
                '보드 알림',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: fontSizeLarge,
                ),
              ),
            ),
            SizedBox(
              height: 50,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 10),
                    child: const Text(
                      '맞춤 보드 추천 알림',
                      style: TextStyle(
                        fontSize: fontSizeLarge,
                      ),
                    ),
                  ),
                  Switch(
                    activeColor: Color.fromRGBO(0, 0, 0, 0.05),
                    value: _isChecked3,
                    onChanged: (value) { // 체크하게 되면
                      setState(() {
                        _isChecked3 = value; // 체크한 값(true)로 변경
                      });
                    },
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 50,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 10),
                    child: const Text(
                      '실시간 추천 게시물 알림',
                      style: TextStyle(
                        fontSize: fontSizeLarge,
                      ),
                    ),
                  ),
                  Switch(
                    activeColor: Color.fromRGBO(0, 0, 0, 0.05),
                    value: _isChecked4,
                    onChanged: (value) { // 체크하게 되면
                      setState(() {
                        _isChecked4 = value; // 체크한 값(true)로 변경
                      });
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}