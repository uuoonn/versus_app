// 마이 페이지 항목 리스트 페이지

import 'package:flutter/material.dart';
import 'package:versus_app/components/component_list_item.dart';
import 'package:versus_app/components/pop_up/component_pop_up_choice.dart';
import 'package:versus_app/config/size.dart';
import 'package:versus_app/model/member/member_response.dart';
import 'package:versus_app/pages/my_page/page_member_ask_create.dart';
import 'package:versus_app/pages/my_page/page_member_ask_list.dart';
import 'package:versus_app/pages/my_page/page_my_info.dart';
import 'package:versus_app/pages/my_page/page_notification.dart';

import '../../functions/token_lib.dart';
import '../../repository/repo_member.dart';


class PageMyPage extends StatefulWidget {
  const PageMyPage({
    super.key,
  });


  @override
  State<PageMyPage> createState() => _PageMyPageState();
}

class _PageMyPageState extends State<PageMyPage> {

  MemberResponse? _detail;


  //API에서 멤버 정보 불러오기
  Future<void> _loadMemberDetail() async {
    await RepoMember().getMemberDetail()
        .then((res) => {
      setState(() {
        _detail = res.data;
      })
    });
  }

  //로그아웃
  Future<void> _logout() async {
    TokenLib.logout(context);
  }


  @override
  void initState() {
    super.initState();
    _loadMemberDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title:
        const Text(
          'My Page',
          style: TextStyle(
            fontSize: fontSizeXlarge,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: const Color.fromRGBO(0, 0, 0, 0),

      ),
      body: ListView(
        children: [
          ComponentListItem(color: Colors.black, title: '회원 정보', fontSize: fontSizeSmall, callback: (){}, fontWeight: FontWeight.normal),
          ComponentListItem(color: Colors.black, title: '내 정보', fontSize: fontSizeLarge, callback: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageMyInfo()));
          }, fontWeight: FontWeight.normal),
          ComponentListItem(color: Colors.black, title: '계정 로그아웃', fontSize: fontSizeLarge, callback: (){

            //팝업창
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return ComponentPopUpChoice(
                    title: '로그아웃 하시겠습니까?',
                    contents: '',
                    text1: '예',

                    //로그아웃시
                    callback1: () async {
                      _logout();
                    },

                    text2: '아니요',
                    callback2: () {
                      Navigator.of(context).pop(); // 아니요 누를 경우 팝업창 닫힘
                    });
              },
            );

          }, fontWeight: FontWeight.normal),
          const Divider(thickness: 1,),
          ComponentListItem(color: Colors.black, title: '설정', fontSize: fontSizeSmall, callback: (){}, fontWeight: FontWeight.normal),
          ComponentListItem(color: Colors.black, title: '알림 설정', fontSize: fontSizeLarge, callback: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageNotification()));
          }, fontWeight: FontWeight.normal),
          const Divider(thickness: 1,),
          ComponentListItem(color: Colors.black, title: '1:1 문의', fontSize: fontSizeSmall, callback: (){}, fontWeight: FontWeight.normal),
          ComponentListItem(color: Colors.black, title: '1:1 문의 작성', fontSize: fontSizeLarge, callback: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageMemberAskCreate()));
          }, fontWeight: FontWeight.normal),
          ComponentListItem(color: Colors.black, title: '1:1 문의 내역', fontSize: fontSizeLarge, callback: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageMemberAskList()));
          }, fontWeight: FontWeight.normal),
          const Divider(thickness: 1,),
          ComponentListItem(color: Colors.black, title: '공지 사항', fontSize: fontSizeSmall, callback: (){}, fontWeight: FontWeight.normal),
          ComponentListItem(color: Colors.black, title: '공지 사항', fontSize: fontSizeLarge, callback: (){}, fontWeight: FontWeight.normal),
          ComponentListItem(color: Colors.black, title: '자주 묻는 질문', fontSize: fontSizeLarge, callback: (){}, fontWeight: FontWeight.normal),
        ],
      ),
    );
  }
}