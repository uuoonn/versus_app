// 문의 내역 리스트

import 'package:flutter/material.dart';
import '../../components/component_list_item.dart';
import '../../config/size.dart';

class PageMemberAskList extends StatefulWidget {
  const PageMemberAskList({super.key});

  @override
  State<PageMemberAskList> createState() => _PageMemberAskListState();
}

class _PageMemberAskListState extends State<PageMemberAskList> {
  @override
  Widget build(BuildContext context) {

    double screenWidth = MediaQuery.of(context).size.width;
    double paddingValue = (screenWidth - 300.0) / 2.0; // 컨텐츠 내용 width 300으로 맞춤

    return Scaffold(
      appBar: AppBar(
        title:
        const Text(
          '1:1 문의 내역',
          style: TextStyle(
            fontSize: fontSizeXlarge,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: const Color.fromRGBO(0, 0, 0, 0),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: paddingValue),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text('회원님의 문의글 리스트입니다.',style: TextStyle(fontSize: fontSizeXlarge, fontWeight: FontWeight.w400),),
            const SizedBox(height: largeGap,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    decoration: BoxDecoration(border: Border.all(color: const Color.fromRGBO(66, 126, 245, 1),)),
                    child: const Text(' 답변 완료 ', style: TextStyle(color: Color.fromRGBO(66, 126, 245, 1),),)),
                const SizedBox(width: largeGap,),
              ],
            ),
            ComponentListItem(color: Colors.black, title: 'Q. 닉네임 변경하고 싶어요.', fontSize: fontSizeLarge, callback: (){}, fontWeight: FontWeight.normal),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    decoration: BoxDecoration(border: Border.all(color: const Color.fromRGBO(66, 126, 245, 1),)),
                    child: const Text(' 답변 완료 ', style: TextStyle(color: Color.fromRGBO(66, 126, 245, 1),),)),
                const SizedBox(width: largeGap,),
              ],
            ),
            ComponentListItem(color: Colors.black, title: 'Q. 아이디를 여러개 쓸 수 있나요?', fontSize: fontSizeLarge, callback: (){}, fontWeight: FontWeight.normal),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    decoration: BoxDecoration(border: Border.all(color: const Color.fromRGBO(66, 126, 245, 1),)),
                    child: const Text(' 답변 완료 ', style: TextStyle(color: Color.fromRGBO(66, 126, 245, 1),),)),
                const SizedBox(width: largeGap,),
              ],
            ),
            ComponentListItem(color: Colors.black, title: 'Q. 회원 등급에 대해 알고 싶어요..', fontSize: fontSizeLarge, callback: (){}, fontWeight: FontWeight.normal),
          ],
        ),
      )
    );
  }
}
