// 카테고리 리스트

import 'package:flutter/material.dart';
import 'package:versus_app/components/component_category_item.dart';
import 'package:versus_app/model/enum/enum_category.dart';
import 'package:versus_app/pages/category/page_category_detail.dart';
import 'package:versus_app/pages/bottom_bar_menu/page_index.dart';
import '../../components/bar/component_app_bar_basic.dart';
import '../../model/member/member_response.dart';

class PageCategory extends StatefulWidget {
  const PageCategory({
    super.key,
  });

  @override
  State<PageCategory> createState() => _PageCategoryState();
}

class _PageCategoryState extends State<PageCategory> {

  MemberResponse? _detail;

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar : ComponentAppBarBasic(text: 'Category', onLeadingPressed: (){
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => PageIndex()));
      }),

      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: GridView.builder(
          shrinkWrap: true,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 2 / 1.5,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
          ),
          itemCount: EnumCategory.values.length,
          itemBuilder: (BuildContext ctx, int idx) {
            final categoryItem = EnumCategory.values[idx]; // EnumCategory의 상수에 접근
            return ComponentCategoryItem(
              callback: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => PageCategoryDetail(categoryIndex: idx,))
                );
              },
              categoryItem: categoryItem, // EnumCategory의 인스턴스 전달
            );
          },
        )


      ),
    );
  }
}
