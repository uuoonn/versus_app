// 검색 페이지
// 현재 검색바 컴포넌트로 만들어서 페이지에 넣어놓기만 함

import 'package:flutter/material.dart';
import 'package:versus_app/components/bar/component_search_bar.dart';
import 'package:versus_app/pages/bottom_bar_menu/page_index.dart';
import '../../components/bar/component_app_bar_basic.dart';
import '../../model/member/member_response.dart';

class PageSearch extends StatefulWidget {
  const PageSearch({super.key});

  @override
  State<PageSearch> createState() => _PageSearchState();
}

class _PageSearchState extends State<PageSearch> {

  MemberResponse? detail;

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double paddingValue = (screenWidth - 320.0) / 2.0; // 컨텐츠 내용 width 320으로 맞춤

    return Scaffold(
      appBar : ComponentAppBarBasic(text: 'Search', onLeadingPressed: (){
        Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => PageIndex()),(route) => false,);
      }),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: paddingValue),
        child: const CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: true,
              flexibleSpace: ComponentSearchBar(),
            )
          ],
        ),
      ),
    );
  }
}
