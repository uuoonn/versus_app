// 실시간, 추천 순으로 볼 수 있는 FEED 페이지

import 'package:flutter/material.dart';
import 'package:versus_app/components/board/component_preview_board_item.dart';
import 'package:versus_app/components/component_count_title.dart';
import 'package:versus_app/config/size.dart';
import 'package:versus_app/pages/board/page_board_detail.dart';
import 'package:versus_app/pages/bottom_bar_menu/page_index.dart';
import 'package:versus_app/repository/repo_board.dart';
import '../../components/bar/component_app_bar_basic.dart';
import '../../model/board/board_item.dart';
import '../../model/member/member_response.dart';

class PageFeed extends StatefulWidget {
  const PageFeed({
    Key? key,

  }) : super(key: key);


  @override
  State<PageFeed> createState() => _PageFeedState();
}

class _PageFeedState extends State<PageFeed> with SingleTickerProviderStateMixin {

  //멤버 정보
  MemberResponse? detail;

  //Top10 추천순으로 게시글 받아오기

  //탭바 컨트롤러
  late TabController _tabController;

  //스크롤 컨트롤러
  final _scrollerController = ScrollController();

  List<BoardItem> _top10List = [];

  List<BoardItem> _list = [];
  int _totalItemCount = 0;
  int _totalPage = 1;
  int _currentPage = 1;

  Future<void> _loadRealTimBoards() async {
    if(_currentPage <= _totalPage) {
      await RepoBoard().getBoardPaging(page: _currentPage)
          .then((res) {
        setState(() {
          _totalPage = res.totalPage.toInt();
          _totalItemCount = res.totalCount.toInt();
          _list = [..._list, ...res.list];

          _currentPage++;
        });
      }).catchError((err) {
        debugPrint(err);
      });
    }
  }


  //추천순 top10 게시글 가져오기
  Future<void> _loadRecommendBoards() async {
    await RepoBoard().getLikeTop10()
        .then((res) => {
      setState(() {
        _list = res.list;
      })
    });
  }


//초기화
  @override
  void initState() {
    super.initState();

    //addListener : 계속 주시하겠다. 감시자 역할
    _scrollerController.addListener(() {
      //만약에.. 스크롤 설정값(offset) = 현재 스크롤 위치가 최대일 경우, _loadItems 실행
      if(_scrollerController.offset == _scrollerController.position.maxScrollExtent) {
        _loadItems();
      }
    });

    // TabController 초기화: 탭의 개수와 TickerProvider를 설정
    _tabController = TabController(length: 2, vsync: this);

    //탄생시 한번만 실행되는 것
    _loadItems();
  }

  //새로 고침 할거니 ? 언급안하면 false이다.
  Future<void> _loadItems({bool reFresh = false}) async {
    //새로고침을 할 거라면,
    if (reFresh) {
      setState(() { //원웨이바인딩이기때문에 바뀌게 되면 계속 알려줘야한다. = setState
        _list = []; // 리스트 싹 날리고 (초기화)
        _top10List = [];
        _totalItemCount = 0; //
        _totalPage = 1; // 총 페이지 1로 초기화
        _currentPage = 1; // 현재 페이지 1로 초기화
      });
    }

    if (_tabController.index == 0) {
      await _loadRealTimBoards();
    } else if (_tabController.index == 1) {
      await _loadRecommendBoards();
    }

    if (reFresh) {
      _scrollerController.animateTo(
          0,
          duration: const Duration(microseconds: 3000),
          curve: Curves.easeOut);
    }
  }



  final choice =['실시간', 'Top10']; // 보드 보여주기 방식



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // AppBar 설정: 커스텀 앱바 컴포넌트 사용
      appBar: ComponentAppBarBasic(
        text: 'Feed',
        onLeadingPressed: () {
          // 뒤로가기 버튼을 눌렀을 때, 페이지 이동 및 기존 페이지 삭제
          Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => PageIndex()),
                (route) => false,
          );
        },
      ),
      // DefaultTabController로 TabBar 및 TabBarView 설정
      body: DefaultTabController(
        length: choice.length,
        child: Column(
          children: [
            TabBar(
              controller: _tabController,
              tabs: choice.map((String choice) {
                return Tab(text: choice);
              }).toList(),
              onTap: (index) {
                _loadItems(reFresh: true);
              },
              indicatorColor: const Color.fromRGBO(0, 0, 0, 1),
              indicatorSize: TabBarIndicatorSize.tab,
              indicatorWeight: 2.0,
              labelColor: const Color.fromRGBO(0, 0, 0, 1),
              unselectedLabelColor: const Color.fromRGBO(0, 0, 0, 0.5),
            ),
            if (_tabController.index == 0)
              ComponentCountTitle(count: _totalItemCount, unitName: '개', itemName: '게시글'),
            Expanded(
              child: TabBarView(
                children: [
                  ListView(
                    controller: _scrollerController,
                    children: [
                      if (_list.isNotEmpty)
                        ListView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: _list.length,
                          itemBuilder: (context, idx) {
                            return ComponentPreviewBoardItem(
                              boardItem: _list[idx],
                              callback: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => PageBoardDetail(id: _list[idx].id),
                                ));
                              },
                            );
                          },
                        ),
                      if (_list.isEmpty)
                        const Text('게시글이 없습니다.'),
                    ],
                  ),
                  ListView(
                    controller: _scrollerController,
                    children: [
                      if (_top10List.isNotEmpty)
                        ListView.builder(
                          shrinkWrap: true,
                          physics: const BouncingScrollPhysics(),
                          itemCount: _top10List.length,
                          itemBuilder: (context, idx) {
                            int itemNumber = idx +1;
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                                  color: Colors.grey.withOpacity(0.2),
                                  child: Text('추천 $itemNumber위',
                                  style: TextStyle(fontSize: fontSizeMedium, fontWeight: FontWeight.bold),),
                                ),
                                ComponentPreviewBoardItem(
                                  boardItem: _top10List[idx],
                                  callback: () {
                                    Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => PageBoardDetail(id: _top10List[idx].id),
                                    ));
                                  },
                                ),
                              ],
                            );
                          },
                        ),
                      if (_top10List.isEmpty)
                        const Text('게시글이 없습니다.'),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}



