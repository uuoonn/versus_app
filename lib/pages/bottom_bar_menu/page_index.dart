// 앱바 바텀바 고정 페이지

import 'package:flutter/material.dart';
import 'package:versus_app/model/member/member_response.dart';
import 'package:versus_app/pages/bottom_bar_menu/page_category_index.dart';
import 'package:versus_app/pages/bottom_bar_menu/page_create_post.dart';
import 'package:versus_app/pages/bottom_bar_menu/page_feed.dart';
import 'package:versus_app/pages/bottom_bar_menu/page_my_board.dart';
import 'package:versus_app/repository/repo_member.dart';



class PageIndex extends StatefulWidget {
  const PageIndex({
    super.key,
  });



  @override
  State<PageIndex> createState() => _PageIndexState();
}
class _PageIndexState extends State<PageIndex> {
  MemberResponse? _detail;
  List<Widget> _navIndex = [];

  //초기화
  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  Future<void> _loadDetail() async {
    // RepoMember를 통해 id에 해당하는 MemberResponse를 가져온다.
    await RepoMember()
        .getMemberDetail()
        .then((res) => setState(() {
      _detail = res.data;
      _navIndex = [
        PageFeed(),
        PageCategoryIndex(),
        PageCreatePost(),
        //const PageSearch(),
        if (_detail != null)PageMyBoard(),
      ];
    }));
  }

  int _selectedIndex = 0;

  void _onNavTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _navIndex.isNotEmpty ? _navIndex.elementAt(_selectedIndex) : Container(), // _navIndex가 비어 있으면 Container()를 반환한다.
      bottomNavigationBar: Container(
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        child: BottomNavigationBar(
          currentIndex: _selectedIndex,
          onTap: _onNavTapped,
          type: BottomNavigationBarType.shifting,
          backgroundColor: Colors.white,
          selectedItemColor: const Color.fromRGBO(0, 0, 0, 1),
          unselectedItemColor: const Color.fromRGBO(0, 0, 0, 0.3),
          showUnselectedLabels: true,
          items: const [
            BottomNavigationBarItem(icon: Icon(Icons.dynamic_feed_rounded),label: 'Feed'),
            BottomNavigationBarItem(icon: Icon(Icons.grid_3x3),label: 'Category'),
            BottomNavigationBarItem(icon: Icon(Icons.add),label: 'New'),
            //BottomNavigationBarItem(icon: Icon(Icons.search_rounded), label: 'Search' ),
            BottomNavigationBarItem(icon: Icon(Icons.dashboard),label: 'My board'),
          ],
        ),
      ),
    );
  }
}
