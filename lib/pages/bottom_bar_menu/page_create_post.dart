//보드 작성 페이지 ( 토픽a 토픽b )

import 'package:flutter/material.dart';
import 'package:versus_app/pages/bottom_bar_menu/page_index.dart';
import '../../components/bar/component_app_bar_basic.dart';
import '../../components/form_field/component_text_form.dart';
import '../../components/pop_up/component_pop_up_alert.dart';
import '../../components/pop_up/component_pop_up_choice.dart';
import '../../config/path.dart';
import '../../config/size.dart';
import '../../model/enum/enum_category.dart';
import '../../model/member/member_response.dart';

class PageCreatePost extends StatefulWidget {
  const PageCreatePost({
    super.key,

  });

  @override
  State<PageCreatePost> createState() => _PageCreatePostState();
}

class _PageCreatePostState extends State<PageCreatePost> {

  MemberResponse? _detail;
  String? selectedValue;

  //API 게시글 작성하기
  // Future<void> _createBoard() async {
  //   await RepoBoard().setCreateBoard()
  //       .then((res) => {
  //   });
  // }
  //
  // @override
  // void initState() {
  //   super.initState();
  //   _createBoard();
  // }


  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double paddingValue = (screenWidth - 300.0) / 2.0;

    return Scaffold(
      appBar : ComponentAppBarBasic(text: 'New', onLeadingPressed: (){
        Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => PageIndex()),(route) => false,);
      }),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: paddingValue),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(
              width: 150,
              child: DropdownButtonFormField(
                isExpanded: true,
                borderRadius: BorderRadius.circular(circularRadiusBase),
                decoration: InputDecoration(
                    contentPadding: const EdgeInsets.fromLTRB(10, 0, 20, 0),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(circularRadiusBase),
                    )
                ),
                hint: Container(
                  padding: const EdgeInsets.fromLTRB(15, 0, 10, 0),
                  child: const Text (
                    '카테고리를 선택하세요',
                    style: TextStyle(fontSize: fontSizeMedium),
                  ),
                ),
                items: EnumCategory.values.map((e) {
                  return DropdownMenuItem<String>(
                    value: e.name,
                    child: Text(
                      e.name,
                      style: const TextStyle(
                        fontSize: fontSizeMedium,
                      ),
                    ),
                  );
                }).toList(),

                validator: (value) {
                  if (value == null) {
                    return '카테고리를 선택해 주세요.';
                  }
                  return null;
                },
                onChanged: (dynamic value) {},
                onSaved: (value){
                  selectedValue = value.toString();
                },

              ),
            ),
            const SizedBox(height: mediumGap,),
            const ComponentTextForm(
              title: '토픽A ',
              width: 300,
              height: 100,
              obscureText: false,
              hintText: '토픽A를 입력해주세요(필수)', lines: 2,),

            const SizedBox(height: mediumGap,),
            SizedBox(
              width: 150,
              height: 46,
              child: OutlinedButton(
                  style: FilledButton.styleFrom(
                    backgroundColor: Colors.white,
                  ),
                  onPressed: (){},
                  child:const Text(
                    '이미지 첨부',
                    style: TextStyle(
                      fontSize: fontSizeMedium,
                      color: Color.fromRGBO(0, 0, 0, 0.5),
                    ),)),),
            const SizedBox(height: largeGap,),

            Container(
                alignment: Alignment.center,
                child: Image.asset('${pathLogo}logo_black.png',
                  width: 50,
                  height: 50,)
            ),
            const SizedBox(height: mediumGap,),

            const ComponentTextForm(
              title: '토픽B ',
              width: 300,
              height: 100,
              obscureText: false,
              hintText: '토픽B를 입력해주세요(필수)', lines: 2,),
            const SizedBox(height: mediumGap,),
            SizedBox(
              width: 150,
              height: 46,
              child: OutlinedButton(
                  style: FilledButton.styleFrom(
                    backgroundColor: Colors.white,
                  ),
                  onPressed: (){},
                  child:const Text(
                    '이미지 첨부',
                    style: TextStyle(
                      fontSize: fontSizeMedium,
                      color: Color.fromRGBO(0, 0, 0, 0.5),
                    ),)),),
            const SizedBox(height: largeGap,),
            SizedBox(
              width: 150,
              height: 46,
              child: TextButton(
                  onPressed: (){
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return ComponentPopUpChoice(
                            title: '등록하시겠습니까?',
                            contents: '',
                            text1: '예',
                            callback1: () {
                              // _createBoard();
                              showDialog(context: context, builder: (BuildContext context) {
                                return ComponentPopUpAlert(title: '게시글이 업로드 되었습니다!', contents: '', text: '닫기', callback: (){
                                  Navigator.of(context).pushAndRemoveUntil(
                                    MaterialPageRoute(builder: (context) => PageIndex()),(route) => false,);
                                });});
                            },
                            text2: '아니요',
                            callback2: () {
                              Navigator.of(context).pop(); // 아니요 누를 경우 팝업창 닫힘
                            });
                      },
                    );
                  },
                  child:const Text('등록')),
            ),
          ],
        ),
      ),
    );
  }
}

