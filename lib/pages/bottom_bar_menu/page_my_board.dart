// 마이 보드 페이지

import 'package:flutter/material.dart';
import 'package:versus_app/components/bar/component_app_bar_basic.dart';
import 'package:versus_app/components/board/component_preview_post_item.dart';
import 'package:versus_app/components/board/component_preview_recommend_item.dart';
import 'package:versus_app/components/board/component_preview_vote_item.dart';
import 'package:versus_app/model/recommend_board/recommend_board_item.dart';
import 'package:versus_app/model/vote/vote_board_item.dart';
import 'package:versus_app/pages/bottom_bar_menu/page_index.dart';
import 'package:versus_app/pages/my_page/page_my_page.dart';
import 'package:versus_app/config/size.dart';
import 'package:versus_app/repository/repo_board.dart';
import 'package:versus_app/repository/repo_member.dart';
import 'package:versus_app/repository/repo_vote_board.dart';
import '../../components/board/component_preview_board_item.dart';
import '../../model/board/board_item.dart';
import '../../model/member/member_response.dart';
import '../../repository/repo_recommend_board.dart';
import '../board/page_board_detail.dart';



class PageMyBoard extends StatefulWidget {
  const PageMyBoard({
    super.key,
  });

  //멤버 id값


  @override
  State<PageMyBoard> createState() => _PageMyBoardState();
}

class _PageMyBoardState extends State<PageMyBoard> {

  //멤버 세부 정보
  MemberResponse? detail;

  //투표 보드 세부 정보
  List<VoteBoardItem> voteList = [];
  int _voteTotalCount = 0;

  //추천 보드 세부 정보
  List<RecommendBoardItem> recommendBoardList = [];
  int _totalRecommendCount = 0;

  //스크롤 컨트롤러
  final _scrollerController = ScrollController();

  List<BoardItem> _list = [];
  int _totalItemCount = 0;
  int _totalPage = 1;
  int _currentPage = 1;


  //API에서 멤버 정보 불러오기
  Future<void> _loadDetail() async {
    await RepoMember().getMemberDetail()
        .then((res) => {
      setState(() {
        detail = res.data;
      })
    });
  }

  //API에서 해당 멤버 투표 정보 불러오기
  Future<void> _loadVoteBoardItem() async {
    await RepoVoteBoard().getVotes()
        .then((res) => {
      setState(() {
        voteList = res.list;
        _voteTotalCount = res.totalCount; // totalCount 업데이트
      })
    });
  }

  // API에서 해당 멤버 추천 게시글 정보 불러오기
  Future<void> _loadRecommendBoardItem() async {
    await RepoRecommendBoard().getRecommendBoards()
        .then((res) => {
      setState(() {
        recommendBoardList = res.list;
        _totalRecommendCount = res.totalCount; // totalCount 업데이트
      })
    });
  }


  //새로 고침 할거니 ? 언급안하면 false이다.
  Future<void> _loadItems({bool reFresh = false}) async {
    //새로고침을 할 거라면,
    if (reFresh) {
      setState(() { //원웨이바인딩이기때문에 바뀌게 되면 계속 알려줘야한다. = setState
        _list = []; // 리스트 싹 날리고 (초기화)
        _totalItemCount = 0; //
        _totalPage = 1; // 총 페이지 1로 초기화
        _currentPage = 1; // 현재 페이지 1로 초기화
      });
    }
    //만약에 .. 현재 페이지가 총 페이지보다 작거나 같을 때
    if(_currentPage <= _totalPage) {
      await RepoBoard().getPersonalBoardPaging(page: _currentPage)
          .then((res) {
        setState(() {
          _totalPage = res.totalPage.toInt();
          _totalItemCount = res.totalCount.toInt();
          _list = [..._list, ...res.list];

          _currentPage++;
        });
      }).catchError((err) {
        debugPrint(err);
      });
    }

    if (reFresh) {
      _scrollerController.animateTo(
          0,
          duration: const Duration(microseconds: 3000),
          curve: Curves.easeOut);
    }
  }
//초기화
  @override
  void initState() {
    super.initState();
    _loadDetail();

    //addListener : 계속 주시하겠다. 감시자 역할
    _scrollerController.addListener(() {
      //만약에.. 스크롤 설정값(offset) = 현재 스크롤 위치가 최대일 경우, _loadItems 실행
      if(_scrollerController.offset == _scrollerController.position.maxScrollExtent) {
        _loadItems();
      }
    });

    //탄생시 한번만 실행되는 것
    _loadItems(reFresh: true);
    _loadVoteBoardItem();
    _loadRecommendBoardItem();
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppBarBasic(
        //앱바에 멤버 닉네임 표시
          text: detail?.nickName.toString() ?? '', //null 체크 추가
          onLeadingPressed: () {
            //앱바 로고 아이콘 클릭시 FEED 페이지로 이동
            //현재 페이지 버리고 가기때문에 뒤로가기 버튼 생성 안됨
            Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) =>
                  PageIndex()), (route) => false,);
          }
      ),
      body: SingleChildScrollView(
        controller: _scrollerController,
        child: _buildBody(context),
      ),
    );
  }

  //바디에 들어가는 부분
  Widget _buildBody(BuildContext context) {

    //MediaQuery width size
    double screenWidth = MediaQuery.of(context).size.width;
    double paddingValue = (screenWidth - 320.0) / 2.0; // 컨텐츠 내용 width 300으로 맞춤


    //상세 정보가 로딩이 되지 않음을 확인 하는 부분
    //detail(멤버 정보)이 null인지 확인
    if (detail == null) {

      //만약 true인 경우 로딩 중임을 알리는 CircularProgressIndicator 표시
      return const Center(child: CircularProgressIndicator());
    } else {
      // false인 경우, 실행 코드
      return Container(
        //MediaQuery
        padding: EdgeInsets.symmetric(horizontal: paddingValue),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 100,
                  child: Column(
                    children: [
                      // 프로필 이미지
                      SizedBox(
                        child: Image.network(
                          detail!.imgUrl,
                          width: 100,
                          height: 100,
                        ),
                      ),
                      // 프로필 이미지 하단 멤버 닉네임
                      Container(
                        margin: const EdgeInsets.all(5),
                        child:  Text(
                          detail!.nickName.toString(),
                          style: const TextStyle(
                            fontSize: fontSizeLarge,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: largeGap,),
                Column(
                  children: [
                    Row(
                      children: [
                        Column(
                          children: [
                            SizedBox(
                              width: 180,
                              child: Row(
                                children: [
                                  // Post 박스
                                  GestureDetector(
                                    child: SizedBox(
                                      width: 45,
                                      height: 60,
                                      child: Column(
                                        children: [
                                          const Text(
                                            'Post',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: fontSizeLarge
                                            ),
                                          ),
                                          Text(_totalItemCount.toString()),
                                        ],
                                      ),
                                    ),
                                    onTap: () {
                                      _showPostBottomSheet(context);
                                    },
                                  ),
                                  const SizedBox(width: largeGap,),
                                  // Vote 박스
                                  GestureDetector(
                                    child: SizedBox(
                                      width: 45,
                                      height: 60,
                                      child: Column(
                                        children: [
                                          const Text(
                                            'Vote',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: fontSizeLarge
                                            ),
                                          ),
                                          Text(_voteTotalCount.toString())
                                        ],
                                      ),
                                    ),
                                    onTap: () {
                                      _showVoteBottomSheet(context);
                                    },
                                  ),
                                  const SizedBox(width: largeGap,),

                                  // Like 박스
                                  GestureDetector(
                                    child: SizedBox(
                                      width: 45,
                                      height: 60,
                                      child: Column(
                                        children: [
                                          const Text(
                                            'Like',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: fontSizeLarge
                                            ),
                                          ),
                                          Text(_totalRecommendCount.toString())
                                        ],
                                      ),
                                    ),
                                    onTap: () {
                                      _showLikeBottomSheet(context);
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    // 마이페이지 버튼
                    SizedBox(
                      width: 140,
                      height: 46,
                      child: TextButton(
                          onPressed: (){
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageMyPage()));
                          },
                          child: const Text('마이 페이지')),
                    ),
                  ],
                ),
              ],
            ),
            //해당 멤버가 쓴 게시글 리스트
            Column(
              children: [
                const SizedBox(height: largeGap,),
                _buildUserPersonalPage(),
              ],
            )
          ],
        ),
      );
    }
  }
  Widget _buildUserPersonalPage() {
    if(_totalItemCount > 0) {
      return ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: _list.length,
          itemBuilder: (BuildContext context, int idx) {
            return ComponentPreviewBoardItem(boardItem: _list[idx], callback: (){
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageBoardDetail(id: _list[idx].id)));
            });
          }
      );
    } else {
      return Container(
        //게시글이 없을 때 화면
        //시간이 된다면 컴포넌트로 만들어서 처리하기
        height: 400,
        alignment: Alignment.center,
        child: Text(
          '${detail!.nickName}님의 게시물 없음',
          textAlign: TextAlign.center,
        ),
      );
    }
  }

  //포스트 바텀 시트 내용 부분
  void _showPostBottomSheet(BuildContext context) {
    showModalBottomSheet(
      backgroundColor: Colors.white,
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        if(_totalItemCount>0){
          return Container(
            height: MediaQuery.of(context).size.height*0.75, // bottomsheet 높이 고정
            padding: const EdgeInsets.all(16.0),
            child: SingleChildScrollView(
              child: Center(
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        IconButton(
                          icon: const Icon(Icons.close),
                          onPressed: () => Navigator.of(context).pop(),
                        ),
                      ],
                    ),
                    Text('${detail?.nickName}님이 올린 게시글이 $_totalItemCount개 있습니다.',style: const TextStyle(fontWeight: FontWeight.bold),),
                    const SizedBox(height: mediumGap,),
                    ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: _list.length > _list.length ? _list.length : _list.length,
                      itemBuilder: (BuildContext context, int idx) {
                        return ComponentPreviewPostItem(boardItem: _list[idx], callback: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageBoardDetail(id: _list[idx].id)));
                        });
                      },
                    ),
                  ],
                ),
              ),
            ),
          );
        } else {
          return Container(
            //게시글이 없을 때 화면
            //시간이 된다면 컴포넌트로 만들어서 처리하기
            height: 400,
            alignment: Alignment.center,
            child: Text(
              '${detail!.nickName}님의 게시물이 없습니다.',
              textAlign: TextAlign.center,
            ),
          );
        }
      },
    );
  }

  //Vote 바텀 시트 내용 부분
  void _showVoteBottomSheet(BuildContext context) {
    showModalBottomSheet(
      backgroundColor: Colors.white,
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        if(_voteTotalCount > 0){
          return Container(
            height: MediaQuery.of(context).size.height * 0.75,
            padding: const EdgeInsets.all(16.0),
            child: SingleChildScrollView(
              child: Center(
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        IconButton(
                          icon: const Icon(Icons.close),
                          onPressed: () => Navigator.of(context).pop(),
                        ),
                      ],
                    ),
                    Text('${detail?.nickName}님이 투표한 게시글이 $_voteTotalCount개 있습니다.',style: const TextStyle(fontWeight: FontWeight.bold),),
                    const SizedBox(height: mediumGap,),
                    ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: voteList.length,
                      itemBuilder: (BuildContext context, int idx) {
                        return ComponentPreviewVoteItem( voteBoardItem: voteList[idx],callback: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageBoardDetail(id: _list[idx].id)));
                        }, );
                      },
                    ),
                  ],
                ),
              ),
            ),
          );
        } else {
          return Container(
            //게시글이 없을 때 화면
            //시간이 된다면 컴포넌트로 만들어서 처리하기
            height: 400,
            alignment: Alignment.center,
            child: Text(
              '${detail!.nickName}님이 투표한 게시글이 없습니다.',
              textAlign: TextAlign.center,
            ),
          );
        }
      } ,
    );
  }


  // 라이크 바텀 시트 부분
  void _showLikeBottomSheet(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.white,
      builder: (BuildContext context) {
        if(_totalRecommendCount > 0) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.75, // BottomSheet의 높이를 고정
            padding: const EdgeInsets.all(16.0),
            child: SingleChildScrollView(
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        IconButton(
                          icon: const Icon(Icons.close),
                          onPressed: () => Navigator.of(context).pop(),
                        ),
                      ],
                    ),
                    Text(
                      '${detail?.nickName}님이 좋아요한 게시글이 $_totalRecommendCount개 있습니다.',
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(height: mediumGap,),
                    ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: recommendBoardList.length,
                      itemBuilder: (BuildContext context, int idx) {
                        return ComponentPreviewRecommendItem(
                          recommendBoardItem: recommendBoardList[idx],
                          callback: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageBoardDetail(id: _list[idx].id)));
                          },
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          );
        } else {
          return Container(
            //게시글이 없을 때 화면
            //시간이 된다면 컴포넌트로 만들어서 처리하기
            height: 400,
            alignment: Alignment.center,
            child: Text(
              '${detail!.nickName}님이 좋아요 한 게시글이 없습니다.',
              textAlign: TextAlign.center,
            ),
          );
        }
      },
    );
  }



}