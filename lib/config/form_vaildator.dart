const String formErrorRequired = '이 값은 필수입니다.';
const String formErrorNumeric = '숫자만 입력해주세요';
const String formErrorEmail = '올바른 이메일형식이 아닙니다.';
const String formErrorEqualPassword = '비밀번호가 일치하지않습니다.';

String formErrorMinNum(int num) => '$num 이상 입력해주세요.';
String formErrorMaxNum(int num) => '$num 이하 입력해주세요.';
String formErrorMinLength(int num) => '$num자 이상 입력해주세요.';
String formErrorMaxLength(int num) => '$num자 이하 입력해주세요.';
