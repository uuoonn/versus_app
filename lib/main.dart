import 'package:flutter/material.dart';
import 'package:versus_app/pages/start/page_intro_screen.dart';
import 'package:versus_app/pages/start/page_main.dart';



void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Versus App',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.white),
        useMaterial3: true,
        textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(
            backgroundColor: const Color.fromRGBO(0, 0, 0, 1),
            foregroundColor: const Color.fromRGBO(255, 255, 255, 1),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
            ),
            minimumSize: const Size(300, 60),
          ),
        ),
      ),
      home: FutureBuilder(
        future: Future.delayed(const Duration(seconds: 3), () => "Intro Completed."),
        builder: (context, snapshot) {
          return AnimatedSize(
              duration: const Duration(milliseconds: 1000),
              child: _splashLoadingWidget(snapshot)
          );
        },
      ),
    );
  }

  Widget _splashLoadingWidget(AsyncSnapshot<Object?> snapshot) {
    if(snapshot.hasError) {
      return const Text("Error!!"); // 스냅샷에 오류가 있다면 에러 메세지 반환
    }
    else if(snapshot.hasData) {
      return const PageMain(); // 스냅샷에 데이터가 있다면 메인페이지로 이동
    }
    else {
      return const PageIntroScreen(); //그 외에는 인트로 스크린으로 이동
    }
  }
}
